package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@ResponseBody
public class TestController {
    @RequestMapping("/hi") //路由注册
    public String sayHi(String name) {
        if (!StringUtils.hasLength(name)) {
            name = "张三";
        }
        return "您好，"+name;
    }
}
