import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:HttpServletResponse之设置状态码
 * User: 15158
 * Date: 2022-11-21
 * Time: 22:28
 */
@WebServlet("/status")
public class StatusServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String status = req.getParameter("status");
//        if(status!=null) {
//            resp.setStatus(Integer.parseInt(status));
//        }
//        resp.getWriter().write("status: "+status);

        String type = req.getParameter("type");
        if(type.equals("1")) {
            resp.setStatus(200);
        }else if(type.equals("2")) {
            resp.setStatus(404);//浏览器自定义格式的404页面
            //resp.sendError(404);//sendError为Tomcat定义的404页面
        }else if(type.equals("3")){
            resp.setStatus(500);
        }else {
            resp.setStatus(502);
        }
    }
}
