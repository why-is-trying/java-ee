import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created with IntelliJ IDEA.
 * Description:打印请求信息
 * User: 15158
 * Date: 2022-11-20
 * Time: 17:44
 */
@WebServlet("/showRequest")
public class ShowRequest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置在浏览器中响应的格式，以防乱码
        resp.setContentType("text/html; charset=utf-8");
        //使用一个字符桶接收在req中得到的数据
        StringBuilder reqBody = new StringBuilder();

        //获取req的协议格式
        System.out.println(req.getProtocol());
        reqBody.append(req.getProtocol());
        reqBody.append("<br>");

        //获取req的请求方法
        System.out.println(req.getMethod());
        reqBody.append(req.getMethod());
        reqBody.append("<br>");

        //获取req的URI
        System.out.println(req.getRequestURI());
        reqBody.append(req.getRequestURI());
        reqBody.append("<br>");

        //获取req的content-path
        System.out.println(req.getContextPath());
        reqBody.append(req.getContextPath());
        reqBody.append("<br>");

        //获取req的queryString,没有则返回null
        System.out.println(req.getQueryString());
        reqBody.append(req.getQueryString());
        reqBody.append("<br>");

        reqBody.append("<h3>headers: </h3>");
        //获取req中的所有请求头名，是一个String对象的枚举类型
        Enumeration<String> enumeration =req.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            //遍历枚举中的每个请求头名
            String headerName = enumeration.nextElement();
            reqBody.append(headerName + ": ");

            //根据请求头名得到请求头的值
            reqBody.append(req.getHeader(headerName));
            reqBody.append("<br>");
        }

        //将获取的信息写回响应
        resp.getWriter().write(reqBody.toString());
    }
}
