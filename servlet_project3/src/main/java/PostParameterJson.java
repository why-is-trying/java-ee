import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * Description:获取post请求中的参数-JSON
 * User: 15158
 * Date: 2022-11-21
 * Time: 10:42
 */
@WebServlet("/postParameterJson")
public class PostParameterJson extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置请求解析时的字符类型
        req.setCharacterEncoding("utf-8");
        //设置响应格式和字符类型
        resp.setContentType("text/html; charset=utf-8");

        //获取req的body长度
        int contentLength = req.getContentLength();
        //构造长度为contentLength的字节数组
        //由于getContentLength是以字节为单位返回的请求长度，并提供输入流
        byte[] buffer = new byte[contentLength];
        InputStream inputStream = req.getInputStream();
        inputStream.read(buffer);

        System.out.println(new String(buffer));
        resp.getWriter().write(new String(buffer));
    }
}
