import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:HttpServletResponse之重定向
 * User: 15158
 * Date: 2022-11-22
 * Time: 10:00
 */
@WebServlet("/redirect")
public class RedirectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //方法一
        //resp.setStatus(302);
        //resp.setHeader("Location","https://blog.csdn.net/qq_45283185?spm=1011.2415.3001.5343");
        //方法二
        resp.sendRedirect("https://www.sogou.com");
    }
}
