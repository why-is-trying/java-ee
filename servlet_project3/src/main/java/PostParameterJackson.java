import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Description:获取post请求参数-引入Jackson依赖
 * User: 15158
 * Date: 2022-11-21
 * Time: 11:56
 */
class Course {
    public ArrayList<String> courses;
}
class Student {
    public String studentId;
    public String studentName;
    public Course course;
}
@WebServlet("/postParameterJackson")
public class PostParameterJackson extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //创建ObjectMapper对象，它是Jackson中的核心类
        ObjectMapper objectMapper = new ObjectMapper();
        //1.调用ObjectMapper中的方法
        //readValue方法 将字符串转为对象;
        //writeValueAsString方法 将对象转为JSON格式字符串

        //读取输入流，获取到要解析的字符串
        //把字符串按照JSON格式解析，得到一组键值对（Map）
        //根据类对象，创建一个是咧
        //遍历类对象中的属性的名字，根据名字在Map中查询到value赋值到对应对象的属性中
        Student student = objectMapper.readValue(req.getInputStream(),Student.class);

        System.out.println("id: "+student.studentId+", name:"+student.studentName+", "+student.course.courses);
//        resp.setContentType("text/html; charset=utf-8");
//        resp.getWriter().write(student.studentId+", "+student.studentName);
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(objectMapper.writeValueAsString(student));
    }
}
