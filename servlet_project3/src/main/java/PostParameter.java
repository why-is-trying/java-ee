import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created with IntelliJ IDEA.
 * Description:获取post请求的参数-form表单格式
 * User: 15158
 * Date: 2022-11-21
 * Time: 10:01
 */
@WebServlet("/postParameter")
public class PostParameter extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置请求解析时的编码类型
        req.setCharacterEncoding("utf-8");
        //设置响应时的编码格式和类型
        resp.setContentType("text/html; charset=utf-8");
        //根据参数名获取参数值
        String userId = req.getParameter("userId");
        String userName = req.getParameter("userName");
        System.out.println("userId= "+userId+"; "+"userName= "+userName);
        resp.getWriter().write("userId= "+userId+", "+"userName= "+userName);

        resp.getWriter().write("<br>"+"values:"+"<br>");
        //获取所有参数名
        Enumeration<String> enumeration = req.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String names = enumeration.nextElement();
            //根据参数名获取参数值
            resp.getWriter().write(req.getParameter(names)+"<br>");
        }
    }
}
