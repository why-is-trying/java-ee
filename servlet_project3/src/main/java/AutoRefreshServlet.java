import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * Description:HttpServletResponse之自动冲刷新
 * User: 15158
 * Date: 2022-11-21
 * Time: 22:57
 */
@WebServlet("/autoRefreshServlet")
public class AutoRefreshServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置响应格式
        resp.setContentType("text/html; charset=utf-8");
        //设置自动刷新时长
        resp.setHeader("refresh","1");
        //响应时间戳
        //resp.getWriter().write(System.currentTimeMillis()+"<br>");

        //获取日期类
        Date date = new Date();
        //将时间戳写入响应
        resp.getWriter().write("timeStamp: "+date.getTime()+"<br>");
        //通过SimpleDateFormat格式化日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //得到格式后的日期
        String time = simpleDateFormat.format(date.getTime());
        //将格式化后的日期写入响应
        resp.getWriter().write("date: "+time);
    }
}
