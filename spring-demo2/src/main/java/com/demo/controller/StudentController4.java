package com.demo.controller;

import com.demo.modle.Student;
import com.demo.service.StudentService;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 11:47
 */
@Controller
public class StudentController4 {
    private StudentService studentService;

    @Resource
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void sayHi() {
        studentService.sayHi();
    }
}
