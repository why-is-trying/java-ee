package com.demo.controller;

import com.demo.modle.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 11:30
 */
@Controller
public class StudentController3 {
    //1.@Resource属性注入
//    @Resource(name = "student2")
//    private Student student;

    @Autowired
    private Student student1;

//    @Autowired
//    @Qualifier("student1")
//    private Student student;

    public void sayHi() {
        System.out.println(student1);
    }
}
