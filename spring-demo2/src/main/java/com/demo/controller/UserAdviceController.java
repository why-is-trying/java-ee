package com.demo.controller;

import com.demo.modle.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 16:25
 */
@Controller
public class UserAdviceController {
    @Autowired
    private User user1;
    public void sayHi() {
        user1.setName("李四");
        System.out.println(user1);
    }
}
