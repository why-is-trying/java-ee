package com.demo.controller;

import com.demo.component.ArticleController;
import com.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 8:40
 */
@Controller
public class StudentController {
    //1.使用属性注入的方式获取 Bean
//    @Autowired
//    private StudentService studentService;

    //2.使用Setter注入
//    private StudentService studentService;
//    @Autowired
//    public void setStudentService(StudentService studentService) {
//        this.studentService = studentService;
//    }

    //3.构造方法注入
    private StudentService studentService;
    //当一个类只有一个构造方法时，@Autowired可以省略
    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    public void sayHi() {
        studentService.sayHi();
    }
}
