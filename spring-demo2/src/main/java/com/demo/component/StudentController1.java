package com.demo.component;

import com.demo.modle.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 23:45
 */
@Controller
public class StudentController1 {
    @Bean
    public Student student1() {
        Student student = new Student();
        student.setAge(18);
        student.setName("刘元");
        return student;
    }
    @Bean
    public Student student2() {
        Student student = new Student();
        student.setAge(20);
        student.setName("章子");
        return student;
    }
}
