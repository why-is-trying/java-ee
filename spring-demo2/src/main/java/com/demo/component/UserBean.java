package com.demo.component;

import com.demo.modle.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 15:39
 */
@Component
public class UserBean {
    @Scope("prototype")
    @Bean
    public User user1() {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        user.setPassword("1234");
        return user;
    }
}
