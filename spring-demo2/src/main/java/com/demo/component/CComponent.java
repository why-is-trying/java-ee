package com.demo.component;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class CComponent {
    @PostConstruct
    public void postConstructor() {
        System.out.println("执行了 CComponent");
    }
}
