package com.demo.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 18:35
 */
@Component
public class AComponent {
    @Autowired
    private BComponent bComponent;

    @PostConstruct
    public void postConstructor() {
        System.out.println("执行了 AComponent");
    }

}
