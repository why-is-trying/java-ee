package com.demo.modle;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 15:40
 */
@Data
public class User {
    private int id;
    private String name;
    private String password;
}
