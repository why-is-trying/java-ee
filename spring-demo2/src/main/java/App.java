import com.demo.component.AComponent;
import com.demo.component.ArticleController;
import com.demo.component.BeanLifeComponent;
import com.demo.component.StudentController1;
import com.demo.controller.*;
import com.demo.modle.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 13:28
 */
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        AComponent aComponent = context.getBean("AComponent",AComponent.class);
    }
    public static void main9(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        BeanLifeComponent beanLifeComponent = applicationContext.getBean("myComponent",BeanLifeComponent.class);
        System.out.println("使用Bean");
        beanLifeComponent.preDestroy();

    }
    public static void main8(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        UserController userController = context.getBean("userController",UserController.class);
        UserAdviceController userAdviceController = context.getBean("userAdviceController",UserAdviceController.class);
        userController.sayHi();
        userAdviceController.sayHi();
        userController.sayHi();
    }
    public static void main7(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Student student1 = context.getBean("student1",Student.class);
        Student student2 = context.getBean("student2",Student.class);
        System.out.println(student1);
        System.out.println(student2);
    }
    public static void main6(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        StudentController4 controller4 = context.getBean("studentController4",StudentController4.class);
        controller4.sayHi();
    }
    public static void main5(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        StudentController3 controller3 = context.getBean("studentController3",StudentController3.class);
        controller3.sayHi();
    }
    public static void main4(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        com.demo.controller.StudentController studentController = context.getBean("studentController", StudentController.class);
        studentController.sayHi();
    }
    public static void main3(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //这里的""中取得名字要与Bean中的方法名一致
//        Student student1 = context.getBean("student",Student.class);
//        System.out.println(student1);
        //但是如果给Bean设置了name属性名，则方法名不能使用，需要和设置的方法名一致
        Student s1 = context.getBean("s1",Student.class);
        Student s2 = context.getBean("s2",Student.class);
        System.out.println(s1);
        System.out.println(s2);
    }
    public static void main2(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        ArticleController articleController = context.getBean("articleController",ArticleController.class);
        User user = context.getBean("user",User.class);
        articleController.sayHello("zhangsan");
        user.sayHello();
    }
    public static void main1(String[] args) {
        //1.先得到 Spring 对象
        //BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        //BeanFactory是懒汉模式
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //ApplicationContext是饿汉模式，在获取spring上下文的时候，就会将注入的bean的构造方法全部加载

        //2.从Spring中取出bean对象
        //根据 bean 的名称（标识）获取bean对象
        User user = (User) context.getBean("user");//取user
        User user2 = (User) context.getBean("user2");//取user2
        //根据 bean 类型获取 bean，多个bean时会报错
        //User user3 = context.getBean(User.class);
        //根据bean名称+bean类型获取bean
        User user4 = context.getBean("user",User.class);

        //3.使用Bean（可选）
        user.sayHello();
        user2.sayHello();
        System.out.println(user == user2);
        user4.sayHello();
    }
}
