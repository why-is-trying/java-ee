package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-29
 * Time: 19:28
 */
@RestController
public class UserController {
    private final static String SESSION_KEY = "USER_SESSION_KEY";

    @RequestMapping("/login")
    public String login(HttpSession session) {
        session.setAttribute(SESSION_KEY, "zhangsan");
        return "login success";
    }

    @RequestMapping("/get")
    public String get(HttpServletRequest request) {
        String username = "暂无";
        HttpSession session = request.getSession(false);
        if (session != null) {
            Object userinfo = session.getAttribute(SESSION_KEY);
            if (userinfo != null) {
                return userinfo.toString();
            }
        }
        return username;
    }
}
