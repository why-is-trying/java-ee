package Net.UdpSocket;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:字典服务器
 * User: 15158
 * Date: 2022-10-16
 * Time: 19:18
 */
public class UdpDictionaryServer extends UdpEchoServer {
    private HashMap<String,String> dic = new HashMap<>();
    public UdpDictionaryServer(int port) throws SocketException {
        super(port);
        dic.put("cat","小猫");
        dic.put("dog","小狗");
    }

    @Override
    public String process(String request) {
        return dic.getOrDefault(request,"没有该词");
    }

    public static void main(String[] args) throws IOException {
        UdpDictionaryServer dictionaryServer = new UdpDictionaryServer(9090);
        dictionaryServer.start();
    }
}
