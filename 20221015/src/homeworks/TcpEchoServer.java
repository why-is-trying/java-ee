package homeworks;

import javax.swing.plaf.nimbus.AbstractRegionPainter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * Description:20221016作业一
 * User: 15158
 * Date: 2022-10-17
 * Time: 10:05
 */
public class TcpEchoServer {
    private ServerSocket listenSocket = null;
    public TcpEchoServer(int port) throws IOException {
        listenSocket = new ServerSocket(port);
    }
    public void start() throws IOException {
        System.out.println("服务器启动！");
        //ExecutorService threadPool = Executors.newCachedThreadPool();
        while (true) {
            //1.接受客户端请求
            Socket clientServer = listenSocket.accept();
            //使用多线程处理客户端请求,使用多线程会出现 不知何时使用线程 浪费资源的情况
            //使用线程池
            //2.与客户端建立处理连接
            /*processConnection(clientServer);*/
            Thread t = new Thread(()->{
                try {
                    processConnection(clientServer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            t.start();
            /*threadPool.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        processConnection(clientServer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });*/
        }
    }

    public void processConnection(Socket clientServer) throws IOException {
        System.out.printf("[%s:%d] 客户端上线\n",clientServer.getInetAddress().toString(),clientServer.getPort());
        try (InputStream inputStream = clientServer.getInputStream();
            OutputStream outputStream = clientServer.getOutputStream()) {
            while (true) {
                //1.接受客户端请求
                Scanner scanner = new Scanner(inputStream);
                if(!scanner.hasNext()) {
                    System.out.printf("[%s:%d] 客户端下线\n",clientServer.getInetAddress().toString(),clientServer.getPort());
                    break;
                }
                String request = scanner.next();
                //2.服务端响应客户端计算处理时长
                String response = process(request);
                //3.将响应写回客户端
                PrintStream printStream = new PrintStream(outputStream);
                printStream.println(response);
                //记得刷新,保证数据从网卡发送
                printStream.flush();
                //4.打印响应记录日志
                System.out.printf("[%s:%d] req:%s; res:%s\n",clientServer.getInetAddress().toString(),clientServer.getPort(),request,response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            clientServer.close();
        }
    }

    public String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        TcpEchoServer server = new TcpEchoServer(10010);
        server.start();
    }
}
