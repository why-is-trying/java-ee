package homeworks;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:20221016作业二
 * User: 15158
 * Date: 2022-10-17
 * Time: 13:14
 */
public class TcpDictionaryServer extends TcpEchoServer{
    private HashMap<String,String> dic = new HashMap<>();
    public TcpDictionaryServer(int port) throws IOException {
        super(port);

        dic.put("cat","小猫");
        dic.put("dog","小狗");
        dic.put("pig","小猪");
    }

    @Override
    public String process(String request) {
        return dic.getOrDefault(request,"没有找到该词！");
    }

    public static void main(String[] args) throws IOException {
        TcpDictionaryServer server = new TcpDictionaryServer(10010);
        server.start();
    }
}
