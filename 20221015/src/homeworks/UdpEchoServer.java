package homeworks;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created with IntelliJ IDEA.
 * Description:20221013作业一
 * User: 15158
 * Date: 2022-10-16
 * Time: 20:09
 */
public class UdpEchoServer {
    private DatagramSocket socket = null;
    //构造方法
    public UdpEchoServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }
    //启动服务器
    public void start() throws IOException {
        System.out.println("服务器启动！");
        while (true) {
            //没循环一次接收一次请求
            //1.接收客户端请求
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096],4096);
            socket.receive(requestPacket);
            //将请求转换为字符方便处理
            String request = new String(requestPacket.getData(),0,requestPacket.getLength());

            //2.服务响应处理时间
            String response = process(request);

            //3.将响应写回客户端
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),0,response.getBytes().length,
                    requestPacket.getSocketAddress());
            socket.send(responsePacket);

            //4.打印一个记录的日志
            System.out.printf("[%s %d]; req : %s; res : %s",requestPacket.getAddress().toString(),
                    requestPacket.getPort(),request,response);
        }
    }

    public String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer server = new UdpEchoServer(9696);
        server.start();
    }
}
