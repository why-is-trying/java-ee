package homeworks;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:20221013作业一
 * User: 15158
 * Date: 2022-10-16
 * Time: 20:24
 */
public class UdpEchoClient {
    private DatagramSocket socket = null;
    private String serverIp1;
    private int serverPort1;

    public UdpEchoClient(String serverIp1,int serverPort1) throws SocketException {
        socket = new DatagramSocket();
        this.serverIp1 = serverIp1;
        this.serverPort1 = serverPort1;
    }
    public void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            //1.用户请求服务
            System.out.print("->");
            String request = scanner.next();
            //2.创建一个 UDP 发送给服务器
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(),request.getBytes().length,
                    InetAddress.getByName(this.serverIp1),this.serverPort1);
            socket.send(requestPacket);
            //3.从服务器读取 UDP 响应数据
            DatagramPacket responsePacket = new DatagramPacket(new byte[4096],4096);
            socket.receive(responsePacket);
            String response = new String(responsePacket.getData(),0,responsePacket.getLength());
            //4.打印服务器响应内容
            System.out.println(response);
        }
    }

    public static void main(String[] args) throws IOException {
        UdpEchoClient client = new UdpEchoClient("127.0.0.1",9696);
        client.start();
    }
}
