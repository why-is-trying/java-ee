package homeworks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:20221016作业一
 * User: 15158
 * Date: 2022-10-17
 * Time: 10:06
 */
public class TcpEchoClient {
    private Socket socket = null;
    public TcpEchoClient(String serverIP,int serverPort) throws IOException {
        socket = new Socket(serverIP,serverPort);
    }
    public void start() {
        Scanner scanner = new Scanner(System.in);
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            while (true) {
                //1.用户输入请求
                System.out.print("->");
                String request = scanner.next();
                //2.客户端请求服务端响应
                PrintStream printStream = new PrintStream(outputStream);
                printStream.println(request);
                printStream.flush();
                //3.接收服务端响应
                Scanner respScanner = new Scanner(inputStream);
                String response = respScanner.next();
                //4.将响应显示在终端
                System.out.println(response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws IOException {
        TcpEchoClient client = new TcpEchoClient("127.0.0.1",10010);
        client.start();
    }
}
