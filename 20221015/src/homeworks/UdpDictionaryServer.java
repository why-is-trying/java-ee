package homeworks;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:20221013作业二
 * User: 15158
 * Date: 2022-10-16
 * Time: 20:44
 */
public class UdpDictionaryServer {
    private DatagramSocket socket = null;
    private HashMap<String,String> dictionary = new HashMap<>();
    public UdpDictionaryServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
        dictionary.put("cat","小猫");
        dictionary.put("dog","小狗");
        dictionary.put("pig","小猪");
    }
    public void start() throws IOException {
        System.out.println("服务器启动！");
        while (true) {
            //1.接收客户端请求
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096],4096);
            socket.receive(requestPacket);
            //将请求转为字符串
            String request = new String(requestPacket.getData(),0,requestPacket.getLength());
            //2.服务器计算请求响应时间
            String response = process(request);
            //3.将响应写回客户端
            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),0,response.getBytes().length,
                    requestPacket.getSocketAddress());
            socket.send(responsePacket);
            //4.打印记录的日志
            System.out.printf("[%s %d] req : %s; res : %s\n",requestPacket.getAddress().toString(),
                    requestPacket.getPort(),request,response);
        }
    }

    private String process(String request) {
        return dictionary.getOrDefault(request,"没有找到该词");
    }

    public static void main(String[] args) throws IOException {
        UdpDictionaryServer server = new UdpDictionaryServer(9595);
        server.start();
    }
}
