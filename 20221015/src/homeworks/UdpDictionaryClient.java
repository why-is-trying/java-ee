package homeworks;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:20221013作业二
 * User: 15158
 * Date: 2022-10-16
 * Time: 20:44
 */
public class UdpDictionaryClient {
    private DatagramSocket socket = null;
    private String serverIp;
    private int serverPort;
    public UdpDictionaryClient(String serverIp,int serverPort) throws SocketException {
        socket = new DatagramSocket();
        this.serverIp = serverIp;
        this.serverPort = serverPort;
    }
    public void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            //1.用户输入请求
            System.out.print("->");
            String request = scanner.nextLine();

            //2.创建一个UDP发送给服务器
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(),request.getBytes().length,
                    InetAddress.getByName(this.serverIp),this.serverPort);
            socket.send(requestPacket);

            //3.接受UDP响应
            DatagramPacket responsePacket = new DatagramPacket(new byte[4096],4096);
            socket.receive(responsePacket);
            String response = new String(responsePacket.getData(),0,responsePacket.getLength());
            //4.打印相应内容
            System.out.println(response);
        }
    }

    public static void main(String[] args) throws IOException {
        UdpDictionaryClient client = new UdpDictionaryClient("127.0.0.1",9595);
        client.start();
    }
}
