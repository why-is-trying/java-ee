import java.util.Objects;

class Student implements Comparable<Student> {
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Student o) {
        if (o == null) return 1;
        return this.age - o.age;
    }

//    @Override
//    public int compareTo(Student o) {
//        if (o == null) return 1;
//        return this.age - o.age;
//    }
}
public class ComparableDemo {
    public static void main(String[] args) {
//        Student s1 = new Student("王一", 20);
//        Student s2 = new Student("金木", 19);
//        System.out.println(s1.equals(s2));
        Student s3 = new Student("小王", 19);
        Student s4 = new Student("小刘", 22);
        System.out.println(s3.compareTo(s4)); //小于0 表示s3.age < s4.age
    }
}
