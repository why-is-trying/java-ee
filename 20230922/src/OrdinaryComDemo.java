
public class OrdinaryComDemo {
    public static void main(String[] args) {
        String s1 = "coder";
        String s2 = "coder";
        String s3 = "coder" + s2;
        String s4 = "coder" + "coder";
        String s5 = s1 + s2;
        System.out.println(s3 == s4);;
        System.out.println(s3 == s5);;
        System.out.println(s4 == "codercoder");;
    }
    public static void main1(String[] args) {
        int a = 1;
        int b = 10;
        String s = "aaa";
        String s2 = "aaa";
        String s3 = new String("aaa");
        String s4 = new String("aaa");
        System.out.println(s == s2);
        System.out.println(s.equals(s2));
        System.out.println(s == s3);
        System.out.println(s.equals(s3));
        System.out.println(s3 == s4);
        System.out.println(s3.equals(s4));
    }
}
