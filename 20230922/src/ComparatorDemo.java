interface Comparator<Dog> {
    int compare(Dog o1, Dog o2);
}
class Dog {
    public int age;
    public String name;

    public Dog(int age, String name) {
        this.age = age;
        this.name = name;
    }
}

class AgeComparator implements Comparator<Dog> {

    @Override
    public int compare(Dog o1, Dog o2) {
        if (o1 == o2) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return o1.age - o2.age;
    }
}

class NameComparator implements Comparator<Dog> {

    @Override
    public int compare(Dog o1, Dog o2) {
        if (o1 == o2) return 0;
        if (o1 == null) return -1;
        if (o2 == null) return 1;
        return o1.name.compareTo(o2.name);
    }
}
public class ComparatorDemo {
    public static void main(String[] args) {
        Dog dog1 = new Dog(3, "aba");
        Dog dog2 = new Dog(2, "aaa");
        Dog dog3 = new Dog(3, "acd");

        //年龄比较器
        AgeComparator ageComparator = new AgeComparator();

        //名字比较器
        NameComparator nameComparator = new NameComparator();

        System.out.println(ageComparator.compare(dog1,dog2)); //>0，表示dog1.age > dog2.age
        System.out.println(nameComparator.compare(dog1, dog3)); //
    }
}
