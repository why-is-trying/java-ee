function getLoginStatus() {
    $.ajax({
        type: 'get',
        url: 'login',
        success: function(body) {
            console.log("当前已经登录过了！");
        },
        error: function() {
            console.log("当前未登录！");
            location.assign('blog_login.html');
        }
    });
}