import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:get请求处理博客列表和博客详情页发来的请求；post请求用来处理提交博客功能
 * User: 15158
 * Date: 2022-12-06
 * Time: 20:50
 */
@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    //可以使用ObjectMapper中的writeValueAsString和readValue两个方法
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过get请求实现blog_list和blog_detail的后端服务
        resp.setContentType("application/json; charset=utf-8");
        BlogDao blogDao = new BlogDao();
        //1.获取blogId
        String blogId = req.getParameter("blogId");
        if (blogId == null) {
            //如果query string中没有blogId说明它是blog_list的请求
            //需要返回所有博客
            List<Blog> blogs = blogDao.selectAll();
            resp.getWriter().write(objectMapper.writeValueAsString(blogs));
        } else {
            //如果不为null 则是blog_detail的请求
            //需要返回blogId这篇博客
            Blog blog = blogDao.selectOne(Integer.parseInt(blogId));
            resp.getWriter().write(objectMapper.writeValueAsString(blog));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //通过post请求实现用户提交博客的功能
        //1.获取当前会话
        HttpSession session = req.getSession(false);
        if (session == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("当前未登录，发布失败");
            return;
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("当前未登录，发布失败");
            return;
        }

        //2.得到请求中的参数
        req.setCharacterEncoding("utf8");
        String title = req.getParameter("title");
        String content = req.getParameter("content");

        //3.构造blog对象
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());

        //4.将blog对象插入数据库中
        BlogDao blogDao = new BlogDao();
        blogDao.insert(blog);

        //5.发布成功，重定向到列表页
        resp.sendRedirect("blog_list.html");
    }
}
