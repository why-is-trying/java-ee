import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description:对用户表的增删查改
 * User: 15158
 * Date: 2022-12-06
 * Time: 20:01
 */
public class UserDao {
    //查询--by--用户名
    public User selectByName(String username) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.获取数据源并建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from user where username=?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);
            //3.执行sql
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //查询--by--用户ID
    public User selectById(int userId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "select * from user where userId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //添加--添加新用户
    public void insert(User user) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.获取数据源并建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "insert into user values(null,?,?)";
            //3.执行sql
            statement = connection.prepareStatement(sql);
            statement.setString(1,user.getUsername());
            statement.setString(2,user.getPassword());
            int ret = statement.executeUpdate();
            //4.判断是否添加成功
            if (ret == 1) {
                System.out.println("新用户添加成功！");
            } else {
                System.out.println("新用户添加失败！");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
