import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:对博客表的增删查改
 * User: 15158
 * Date: 2022-12-06
 * Time: 20:00
 */
public class BlogDao {
    //添加：插入一个博客到数据库中
    public void insert(Blog blog) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.获取数据源并连接
            connection = DBUtil.getConnection();
            //2.构造sql语句
            String sql = "insert into blog values(null,?,?,now(),?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, blog.getTitle());
            statement.setString(2,blog.getContent());
            statement.setInt(3,blog.getUserId());
            //3.执行sql
            int ret = statement.executeUpdate();
            if (ret == 1) {
                System.out.println("博客插入成功！");
            }else {
                System.out.println("博客插入失败！");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //4.释放资源
            DBUtil.close(connection,statement,null);
        }
    }

    //查询：根据博客Id查询指定博客
    public Blog selectOne(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.获取数据源并建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL
            String sql = "select * from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //3.执行sql
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            //一定别忘记释放资源
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //查询：查询博客列表页的所有博客
    public List<Blog> selectAll() {
        List<Blog> blogs = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.获取数据源并建立连接
            connection = DBUtil.getConnection();
            //2.构造sql
            String sql = "select * from blog";
            //3.执行sql
            statement = connection.prepareStatement(sql);
            //4.判读结果
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //列表页的博客只需要摘录正文一部分就好
                String content = resultSet.getString("content");
                if (content.length() > 100) {
                    content = content.substring(0,100) + "......";
                }
                blog.setContent(content);
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return blogs;
    }

    //删除：删除指定博客
    public void delete(int blogId) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            String sql = "delete from blog where blogId=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            int ret = statement.executeUpdate();
            if (ret == 1) {
                System.out.println("博客删除成功！");
            } else {
                System.out.println("博客删除失败！");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
