/**
 * Created with IntelliJ IDEA.
 * Description:用户类
 * User: 15158
 * Date: 2022-12-06
 * Time: 19:54
 */
public class User {
    private int userId;//用户Id
    private String username;//用户名
    private String password;//用户密码

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
