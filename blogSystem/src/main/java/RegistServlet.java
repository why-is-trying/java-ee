
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:处理登录页面发送的post请求
 * User: 15158
 * Date: 2022-12-07
 * Time: 11:28
 */
@WebServlet("/regist")
public class RegistServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.从请求中获取用户名和密码
        req.setCharacterEncoding("utf8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //2.判断用户名或密码是否为空
        if (username == null || username.equals("") || password == null || password.equals("")) {
            //用户名或密码输入为空
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户名或密码为空！注册失败！");
            return;
        }
        //3.查询数据库，看是否有相同用户名的用户
        UserDao userDao = new UserDao();
        User user = userDao.selectByName(username);
        if (user != null) {
            //说明数据库中有现在用户注册的用户名
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户名重复！注册失败！");
            return;
        }
        //4.真正的注册
        //向数据库中添加该注册用户
        user = new User();
        user.setUsername(username);
        user.setPassword(password);
        userDao.insert(user);
        //5.构造重定向
        resp.sendRedirect("blog_login.html");
    }
}
