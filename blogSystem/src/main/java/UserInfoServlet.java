import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:实现博客详情页显示博客作者的信息
 * User: 15158
 * Date: 2022-12-06
 * Time: 22:22
 */
@WebServlet("/userInfo")
public class UserInfoServlet extends HttpServlet {
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取用户信息
        String blogId = req.getParameter("blogId");
        //2.判断query string 是否存在blogId
        if (blogId == null) {
            //如果blogId不存在，则说明当前是blog_list页面
            //页面的左边框需要显示的是当前登录用户的信息--从session中获取
            getUserInfoFromSession(req,resp);
        } else {
            //如果blogId存在，则说明当前是blog_detail页面
            //页面的左边框需要显示的是当前博客作者的信息--从数据库中获取
            getUserInfoFromDB(req,resp,Integer.parseInt(blogId));
        }
    }

    private void getUserInfoFromDB(HttpServletRequest req, HttpServletResponse resp, int blogId) throws IOException {
        //1.先根据blogId查看blog对象，获取到博客的userId
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(blogId);
        if (blog == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("blogId不存在");
            return;
        }
        //2.根据userId查询对应的user对象
        UserDao userDao = new UserDao();
        User user = userDao.selectById(blog.getUserId());
        if (user == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("blogId不存在");
            return;
        }
        //3.将用户返回给浏览器
        user.setPassword("");
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(objectMapper.writeValueAsString(user));
    }

    private void getUserInfoFromSession(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //1.获取当前会话
        HttpSession session = req.getSession(false);
        //2.判断会话是否存在
        if (session == null) {
            //不存在则用户未登录
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户未登录");
            return;
        }
        //3.判断用户是否存在
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //不存在则用户未登录
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户未登录");
            return;
        }
        //4.成功,将user返回
        user.setPassword("");
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(objectMapper.writeValueAsString(user));
    }
}
