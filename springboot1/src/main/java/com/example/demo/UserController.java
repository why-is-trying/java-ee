package com.example.demo;

import com.example.demo.model.ReadList;
import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:验证springBoot是否创建成功
 * User: 15158
 * Date: 2022-12-09
 * Time: 18:50
 */
@Controller
public class UserController {

    @Value("${server.port}")
    private String port;

    @Value("${mystring1}")
    private String mystring1;

    @Value("${mystring2}")
    private String mystring2;

    @Value("${mystring3}")
    private String mystring3;

    @Value("${yml.String}")
    private String ymlStr;

    @Autowired
    private Student student1;

    @Autowired
    private ReadList readList;

    @ResponseBody //返回一个非静态页面的数据
    @RequestMapping("/sayhi") //设置路由地址
    public String sayHi() {
        return "hello,world.|port=" + port + " | ymlStr=" + ymlStr;
    }

    @ResponseBody
    @RequestMapping("/sayyes")
    public void sayYes() {
        //yml 中如果使用了双引号就会按原语义执行，如果不加双引号，就会先按照转义后的执行
        System.out.println(mystring1);
        System.out.println(mystring2);
        System.out.println(mystring3);
    }

    @ResponseBody
    @RequestMapping("/saystudent")
    public Student saystudent() {
        return student1;
    }

    @ResponseBody
    @RequestMapping("/sayset")
    public String sayset() {
        return readList.getName().toString();
    }
}
