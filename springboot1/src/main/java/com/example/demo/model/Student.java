package com.example.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2022-12-10
 * Time: 21:30
 */
@Data
@ConfigurationProperties(prefix = "student")
@Component
public class Student {
    private int id;
    private String name;
    private int age;
}
