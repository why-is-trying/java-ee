package com.example.demo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2022-12-10
 * Time: 21:41
 */
@Component
@Data
@ConfigurationProperties(prefix = "dbtypes")
public class ReadList {
    private List<String> name;
}
