package com.example.demo.mapper;

import com.example.demo.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2022-12-20
 * Time: 19:03
 */
@Slf4j
@SpringBootTest
class UserMapperTest {
    @Resource
    private UserMapper userMapper;

    @Test
    void getUserById() {
        UserInfo userInfo = userMapper.getUserById(1);
        Assertions.assertNotNull(userInfo);
    }

    @Test
    @Transactional //在单元测试中添加此注解，表示在方法执行完之后回滚
    void update() {
        int result = userMapper.update(2,"老六");
        Assertions.assertEquals(1,result);
    }

    @Test
    @Transactional
    void delete() {
        int result = userMapper.delete(2);
        Assertions.assertEquals(1,result);
    }

    @Test
    void add() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("李四");
        userInfo.setPassword("123");
        userInfo.setPhoto("default.png");
        int result = userMapper.add(userInfo);
        System.out.println("添加结果："+result);
        Assertions.assertEquals(1,result);
    }

    @Test
    @Transactional
    void addGetId() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("李四");
        userInfo.setPassword("123");
        userInfo.setPhoto("default.png");
        System.out.println("添加之前的id:"+userInfo.getId());
        int result = userMapper.addGetId(userInfo);
        System.out.println("添加之后的id:"+userInfo.getId());
        System.out.println("受影响行数："+result);

        Assertions.assertEquals(1,result);
    }

    @Test
    void getUserByUserName() {
        UserInfo userInfo = userMapper.getUserByUserName("admin");
        Assertions.assertNotNull(userInfo);
    }

    @Test
    void getOrderList() {
        List<UserInfo> list = userMapper.getOrderList("desc");
        log.info("列表："+list);
    }

    @Test
    void login() {
        String username = "admin";
        String password = "admin";
        UserInfo userInfo = userMapper.login(username,password);
        log.info("登录用户信息："+userInfo);
    }

    @Test
    void getListByName() {
        String username = "a";
        List<UserInfo> list = userMapper.getListByName(username);
        log.info("用户信息："+list);
    }

    @Test
    void getUserAndArticleByUid() {
        UserInfo userInfo = userMapper.getUserAndArticleByUid(2);
        log.info("查询详情："+userInfo);
    }

    @Test
    @Transactional
    void add2() {
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("老六");
        userInfo.setPassword("123456");
        userInfo.setPhoto("123.png");
        int ret = userMapper.add2(userInfo);
        log.info("添加用户的结果："+ret);
    }
}