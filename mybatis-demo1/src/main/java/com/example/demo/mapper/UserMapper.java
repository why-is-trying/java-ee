package com.example.demo.mapper;

import com.example.demo.model.ArticleInfo;
import com.example.demo.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2022-12-17
 * Time: 21:57
 */
@Mapper
public interface UserMapper {

    //根据id查询用户
    public UserInfo getUserById(@Param("id") Integer id);

    //根据username进行查询
    public UserInfo getUserByUserName(@Param("username") String username);

    //根据名称模糊查询
    public List<UserInfo> getListByName(@Param("username") String username);

    //根据id修改名称
    public int update(@Param("id") Integer id,@Param("username") String username);

    //根据id删除
    public int delete(@Param("id") Integer id);

    //添加 返回受影响的行数
    public int add(UserInfo userInfo);

    //添加 返回受影响的行数和id
    public int addGetId(UserInfo userInfo);

    //根据创建时间，获取一个排序的列表
    public List<UserInfo> getOrderList(@Param("order") String order);

    //登录功能
    public UserInfo login(@Param("username") String username,@Param("password") String password);

    //一对多查询
    public UserInfo getUserAndArticleByUid(@Param("uid") Integer uid);

    //添加信息--photo
    //动态SQL之if标签
    public int add2(UserInfo userInfo);
}
