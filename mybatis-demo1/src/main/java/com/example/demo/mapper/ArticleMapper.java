package com.example.demo.mapper;

import com.example.demo.model.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2022-12-27
 * Time: 17:15
 */
@Mapper
public interface ArticleMapper {

    //一对一查询
    public ArticleInfo getArticleById(@Param("id") Integer id);
}
