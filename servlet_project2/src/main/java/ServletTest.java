import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:serclet作业-20221117
 * User: 15158
 * Date: 2022-11-18
 * Time: 12:19
 */
@WebServlet("/method")
public class ServletTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("get");
        resp.setContentType("text/html; charset=utf-8");
        resp.getWriter().write("get 响应");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("post");
        resp.setContentType("text/html; charset=utf-8");
        resp.getWriter().write("post 响应");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("delete");
        resp.setContentType("text/html; charset=utf-8");
        resp.getWriter().write("delete 响应");
    }
}
