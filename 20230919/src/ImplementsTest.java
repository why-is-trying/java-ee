
interface IShape {
    void draw();
}
interface IColor {
    void paint();
}
class Circle implements IShape,IColor {

    @Override
    public void draw() {
        System.out.println("画圆");
    }

    @Override
    public void paint() {
        System.out.println("上色");
    }
}
class Triangle implements IShape {
    @Override
    public void draw() {
        System.out.println("画三角形");
    }
}
public class ImplementsTest {
    public static void draw(IShape iShape) {
        iShape.draw();
    }
    public static void main(String[] args) {
        draw(new Circle());
        draw(new Triangle());
    }
}
