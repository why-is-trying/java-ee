abstract class Person {
    String name;
    public Person(String name) {
        this.name = name;
    }
    public void eat() {
        System.out.println(name + "正在吃饭~");
    }
    public abstract void fun();
}
class Student extends Person {

    public Student(String name) {
        super(name);
    }

    @Override
    public void fun() {
        System.out.println(name + "正在上课~");
    }
}
public class AbstractTest {
    public static void main(String[] args) {
        Student student = new Student("小王");
        student.fun();
        student.eat();
    }
}
