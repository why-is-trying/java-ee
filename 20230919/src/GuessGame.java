import java.util.Random;
import java.util.Scanner;

public class GuessGame {
    public static void test1() {
        Scanner in = new Scanner(System.in);
        Random random = new Random();
        int correctNum = random.nextInt(101);//[0,100]
        while (true) {
            System.out.println("请输入您猜的数字：");
            int input = in.nextInt();
            if (input > correctNum) {
                System.out.println("您猜的数字过大！");
            } else if (input < correctNum) {
                System.out.println("您猜的数字过小！");
            } else {
                System.out.println("恭喜您，猜对了~");
                break;
            }
        }
    }
    public static void main(String[] args) {
        test1();
    }
}
