
class Animal {
    String name;
    int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void fun() {
        System.out.println(this.name + "," + this.age + "岁,正在吃~");
    }
    public Animal eat(String name,int age) {
        return new Animal(name, age);
    }
}

class Dog extends Animal {
    public Dog(String name, int age) {
        super(name, age);
    }
    @Override
    public void fun() {
        System.out.println(name + "," + age + "岁,正在跑~");
    }

    public Dog eat(String name,int age) {
        return new Dog(name, age);
    }
}
public class OverrideTest {
    public static void main(String[] args) {
        Dog dog = new Dog("小白", 8);
        Animal dog1 = new Dog("小白", 8);
        Animal dog2 = new Animal("小白", 8);
        dog.fun();
        dog1.fun();
        dog2.fun();
    }
}
