class Test {
    public Test() {
        System.out.println("我是无参构造方法！");
    }
    public Test(int a) {
        System.out.println("我是有一个参数的构造方法！");
    }
    public Test(int a, int b) {
        System.out.println("我是有两个参数的构造方法！");
    }
}
public class OverloadTest {
    public static int add(int x, int y) {
        return x + y;
    }
    public static int add(int x, int y, int z) {
        return x + y + z;
    }
    public static double add(double x, double y) {
        return x + y;
    }
    public static void main(String[] args) {
        System.out.println(add(1,2));
        System.out.println(add(1.0,2.1));
        System.out.println(add(1,2, 3));
    }
}
