package com;

import com.beans.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.security.cert.X509Certificate;

/**
 * Created with IntelliJ IDEA.
 * Description:创建spring项目
 * User: 15158
 * Date: 2022-12-07
 * Time: 20:39
 */
public class App {
    public static void main(String[] args) {
//        //1.得到spring上下文对象--这里面的参数是spring-config.xml的名字
//        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
//        //2.根据上下文对象提供的方法获取到 bean--这的的参数是spring-config.xml中配置的bean的id
//        User user = (User) context.getBean("user");
//        //3.使用bean的方法
//        user.sayHi("王五");

        //1.得到bean工厂
//        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        //2.获取bean
//        User user = (User) factory.getBean("user");
//        //3.使用bean
//        user.sayHi("王五");


        //经典面试题
        //ApplicationContext 和 BeanFactory 的区别
        //相同点：都实现从容器中获取bean，都提供了getBean的方法
        //不同点：（1）ApplicationContext属于BeanFactory的子类，BeanFactory只提供了基础访问bean的方法，
        // 而ApplicationContext除了拥有BeanFactory的所有功能之外，还提供了更多的方法
        //（2）从性能方面来说二者是不同的，BeanFactory是按需加载Bean，ApplicationContext是饿汉方式，在创建时会将所有的Bean都加载起来，以备以后使用。

        //1.使用ApplicationContext就会把Article和User都加载了
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //获取bean的三种方式
        //（1）使用bean的name获取bean
        //User user = (User) context.getBean("user");
        //（2）根据bean的type获取bean容易出现问题（当同一个类型被注入对个的时候就会报错）
        // User user = context.getBean(User.class);
        //（3）根据bean name 和类型获取bean --推荐
        User user = context.getBean("user",User.class);
        user.sayHi("张三");

        //1.使用BeanFactory的时候是懒汉加载方式，只有真正用到哪个bean的时候才会加载
        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        User user2 = (User) factory.getBean("user");
        user2.sayHi("王五");
    }
}
