package com.beans;

/**
 * Created with IntelliJ IDEA.
 * Description:一个bean-普通用户User
 * User: 15158
 * Date: 2022-12-07
 * Time: 20:56
 */
public class User {
    public User() {
        System.out.println("加载了 User()");
    }
    public void sayHi(String name) {
        System.out.println("您好，"+name+"!");
    }
}
