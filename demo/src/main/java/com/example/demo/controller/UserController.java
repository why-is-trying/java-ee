package com.example.demo.controller;

import com.example.demo.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created with IntelliJ IDEA.
 * Description:路由地址
 * UserInfo: 15158
 * Date: 2022-12-13
 * Time: 12:23
 */
//@RestController == @ResponseBody + @Controller
@Slf4j
@Controller
@ResponseBody  //表示返回的为非静态页面
@RequestMapping("/user")  //类的这个可以省略
public class UserController {

    // 从配置文件中读取
    @Value("${img.path}")
    private String imgpath;

    //post、get都支持
    @RequestMapping("/sayhi1")
    public String sayHi() {
        return "你好，世界。"+imgpath;
    }

    //只支持post类型
    @RequestMapping(method = RequestMethod.POST, value = "/sayhi2")
    public String sayHi2() {
        return "你好，世界2。";
    }

    //只支持post请求
    @PostMapping("/sayhi3")
    public String sayHi3() {
        return "你好，世界3。";
    }

    //只支持get请求
    @GetMapping("/sayhi4")
    public String sayHi4() {
        return "你好，世界4，热部署！！！！！！";
    }

    //获取一个参数
    @RequestMapping("getuserbyid")
    public UserInfo getUserById(Integer id) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(id==null?0:id);
        userInfo.setName("张三");
        userInfo.setAge(20);
        return userInfo;
    }

    //获取多个参数
    // @RequestParam 修改前端属性名称
    @RequestMapping("/login")
    public String login(@RequestParam(value = "name",required = false) String username,String password) {
        return "用户名："+username+" | 密码："+password;
    }

    //获取对象
    //@RequestBody 表示从前端接收到的是一个JSON数据
    @RequestMapping("/register")
    public String register(@RequestBody UserInfo userInfo) {
        return "用户信息："+userInfo;
    }

    //从URL地址中获取参数
    @RequestMapping("/getheroinfo/{id}/{name}")
    public String getHeroInfo(@PathVariable String id,@PathVariable String name) {
        return "id:"+id+" | name:"+name;
    }

    //修改图片
    @RequestMapping("/upimg")
    public boolean upImg(Integer uid, @RequestPart("img") MultipartFile file) {
        boolean result = false;
        try {
            file.transferTo(new File("D:/Book/img.jpg"));
            result = true;
        } catch (IOException e) {
            log.error("上传图片失败："+e.getMessage());
        }
        return result;
    }

    //获取cookie
    //方法一：servlet的方法
    @RequestMapping("/cookie")
    public void getCookie(HttpServletRequest request) {
        //得到全部Cookie
        Cookie[] cookies = request.getCookies();
        for (Cookie item : cookies) {
            System.out.println("cookie-name:"+item.getName()+" | cookie-value"+item.getValue());
        }
    }
    //方法二：使用@CookieValue
    @RequestMapping("/cookie2")
    public String getCookie2(@CookieValue("cookie") String cookie) {
        return "cookie: "+cookie;
    }

    //获取header
    //方法一：servlet
    @RequestMapping("/header")
    public String getHeader(HttpServletRequest request) {
        return "header: " + request.getHeader(("User-Agent"));
    }
    //方法二：
    @RequestMapping("/header2")
    public String getHeader2(@RequestHeader("User-Agent") String userAgent) {
        return "userAgent: " + userAgent;
    }

    //session存储
    @RequestMapping("/setsess")
    public String setsess(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        if (session!=null) {
            session.setAttribute("username","java");
        }
        return "存储成功";
    }
    //session获取
    @RequestMapping("/getsess")
    public String getsess(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String username = "null";
        if (session!=null && session.getAttribute("username")!=null) {
            username = (String) session.getAttribute("username");
        }
        return username;
    }
}
