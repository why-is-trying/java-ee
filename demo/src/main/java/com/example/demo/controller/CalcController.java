package com.example.demo.controller;

import com.example.demo.model.LoginInfo;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:加法计算器、登录
 * User: 15158
 * Date: 2022-12-16
 * Time: 16:15
 */
@RestController
public class CalcController {
    //加法计算器
    @RequestMapping("/calc")
    public String calc(Integer num1,Integer num2) {
        if (num1 == null || num2 == null) {
            return "<h3> 输入有误！</h3> <a href=\"http://127.0.0.1:8080/calculator.html\">返回</a>";
        }
        return "<h3> 两数和为："+(num1+num2)+"</h3> <a href=\"http://127.0.0.1:8080/calculator.html\">返回</a>";
    }

    @RequestMapping("/login")
    public HashMap<String, Object> login(String username, String password) {
        HashMap<String, Object> body = new HashMap<>();
        int state = 200;
        int data = -1;
        String msg = "";
        if (StringUtils.hasLength(username) && StringUtils.hasLength(password)) {
            if (username.equals("admin") && password.equals("admin")) {
                data = 1;
                msg = "";
            }else {
                msg = "用户名或密码错误！";
            }
        }else {
            msg = "非法参数";
        }
        body.put("state",state);
        body.put("data",data);
        body.put("msg",msg);
        return body;
    }

    @RequestMapping("/login2")
    public HashMap<String, Object> login2(@RequestBody LoginInfo loginInfo) {
        HashMap<String, Object> body = new HashMap<>();
        int state = 200;
        int data = -1;
        String msg = "";
        if (StringUtils.hasLength(loginInfo.getUsername()) && StringUtils.hasLength(loginInfo.getPassword())) {
            if (loginInfo.getUsername().equals("admin") && loginInfo.getPassword().equals("admin")) {
                data = 1;
                msg = "";
            }else {
                msg = "用户名或密码错误！";
            }
        }else {
            msg = "非法参数";
        }
        body.put("state",state);
        body.put("data",data);
        body.put("msg",msg);
        return body;
    }
}
