package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:请求转发、重定向
 * User: 15158
 * Date: 2022-12-17
 * Time: 17:55
 */
@RestController
public class RefController {
    //请求转发
    //方式一
    @RequestMapping("/fw")
    public String myForward() {
        return "forward:/login.html";
    }
    @RequestMapping("/fw1")
    public String myForward1() {
        return "/login.html";
    }
    //方式二
    @RequestMapping("/fw2")
    public void myForward2(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/login.html").forward(request,response);
    }

    //请求重定向
    @RequestMapping("/rd")
    public String myRedirect() {
        return "redirect:/login.html";
    }
    @RequestMapping("/rd2")
    public void myRedirect2(HttpServletResponse response) throws IOException {
        response.sendRedirect("/login.html");
    }
}
