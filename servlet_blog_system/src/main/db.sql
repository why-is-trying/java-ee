--创建数据库
create database if not exists blogSystem;

--使用数据库
use blogSystem;

--创建博客表
drop table if exits blog;
create table blog (
    blogId int primary key auto_increment,
    title varchar(256),
    content text,
    postTime datetime,
    userId int
);

drop table if exists user;
create table user (
    userId int primary key auto_increment,
    username varchar(50) unique,
    password varchar(50)
);

insert into blog values(null,"第一篇博客","我的博客内容","2022-11-25 11:11:11",1);
insert into blog values(null,"第二篇博客","我的博客内容",now(),1);
insert into blog values(null,"我的博客","我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习，我要好好复习。",now(),2);
insert into blog values(null,'这是markdown博客','###三级标题 \n >引用内容',now(),1);

insert into user values(null,"张三","123");
insert into user values(null,"王一","123");

