import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:处理登录页面发的post请求
 * User: 15158
 * Date: 2022-11-28
 * Time: 20:13
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.从请求中获取用户名和密码
        req.setCharacterEncoding("utf8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //2.判断用户名和密码是否为空
        if(username == null || username.equals("") || password == null || password.equals("")) {
            //说明用户或者密码输入为空
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户名或密码为空！登录失败！");
            return;
        }
        //3.查询数据库，验证用户名和密码是否正确
        UserDao userDao = new UserDao();
        User user = userDao.selectByName(username);
        if (user == null || !user.getPassword().equals(password)) {
            //说明用户不存在或者密码错误
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户不存在或密码错误！登录失败！");
            return;
        }
        //4.如果正确，创建一个会话对象
        HttpSession session = req.getSession(true);
        session.setAttribute("user",user);

        //5.构造302响应报文
        resp.sendRedirect("blog_list.html");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //判定当前登录状态
        //1.获取当前会话
        HttpSession session = req.getSession(false);
        //2.如果会话不存在
        if (session == null) {
            resp.setStatus(403);
            return;
        }
        //3.会话存在，获取user
        User user = (User) session.getAttribute("user");
        //4.判定user是否存在
        if (user == null) {
            resp.setStatus(403);
            return;
        }
        //5.成功，即该会话存在并且user也存在
        //也可以不返回200，默认200
        resp.setStatus(200);
    }
}
