import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:处理博客列表页和博客详情页左边框中的信息
 * User: 15158
 * Date: 2022-11-28
 * Time: 20:53
 */
@WebServlet("/userInfo")
public class UserInfoServlet extends HttpServlet {
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取用户信息
        String blogId = req.getParameter("blogId");
        //2.判断用户是否存在
        if (blogId == null) {
            //在列表页，获取当前用户登录的信息--session中获取
            // 即列表页的左边框中显示的是登录该博客系统的人的信息
            getUserInfoFromSession(req,resp);
        } else {
            //在详情页，获取文章作者的信息--查询数据库
            // 即在博客详情页的左边框显示的是文章作者的信息
            getUserInfoFromDB(req,resp,Integer.parseInt(blogId));
        }
    }

    private void getUserInfoFromDB(HttpServletRequest req, HttpServletResponse resp, int blogId) throws IOException {
        //1.先根据blogId查看blog对象,获取到该博客的userId
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(blogId);
        if (blog == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("blogId不存在");
            return;
        }
        //2.根据userId查询对应的user对象
        UserDao userDao = new UserDao();
        //根据userId获取用户信息
        User user = userDao.selectById(blog.getUserId());
        //判断用户是否存在
        if (user == null) {
            //该用户不存在
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("该用户不存在！");
            return;
        }
        //3.将 user 对象返回给浏览器
        user.setPassword("");//不返回用户密码信息
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(objectMapper.writeValueAsString(user));
     }

    private void getUserInfoFromSession(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //1.获取当前会话
        HttpSession session = req.getSession(false);
        //2.判断会话是否存在
        if(session == null) {
            //不存在则用户未登录
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户未登录");
            return;
        }
        //3.判断用户是否存在
        User user = (User) session.getAttribute("user");
        if (user == null) {
            //用户不存在
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf8");
            resp.getWriter().write("用户不存在");
            return;
        }

        //4.成功，将user对象返回给浏览器
        user.setPassword("");//不返回用户密码信息
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write(objectMapper.writeValueAsString(user));
    }
}
