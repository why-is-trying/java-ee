
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:处理博客列表和博客详情页发送的get请求;post请求用来处理提交博客功能
 * User: 15158
 * Date: 2022-11-26
 * Time: 22:59
 */
@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        BlogDao blogDao = new BlogDao();
        //1.获取blogId
        String blogId = req.getParameter("blogId");
        if(blogId == null) {
            //2.如果为空，则是博客列表页发出的请求
            //返回所有的博客
            List<Blog> blogs = blogDao.selectAll();
            resp.getWriter().write(objectMapper.writeValueAsString(blogs));
        } else {
            //3.如果不为空，则是博客博客详情页的请求，获取blogId这一篇文章
            Blog blog = blogDao.selectOne(Integer.parseInt(blogId));
            resp.getWriter().write(objectMapper.writeValueAsString(blog));
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //实现用户提交博客功能

        //1.获取当前会话
        HttpSession session = req.getSession(false);
        //作用：（1）为了防止，有人使用postman等构造post请求，直接访问；（2）可以利用session找到当前作者是谁
        if (session == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("当前未登录，发布失败！");
            return;
        }
        User user = (User) session.getAttribute("user");
        if (user == null) {
            resp.setStatus(403);
            resp.setContentType("text/html; charset=utf-8");
            resp.getWriter().write("当前未登录，发布失败！");
            return;
        }

        //2.虎丘请求中的参数
        req.setCharacterEncoding("utf8");
        String title = req.getParameter("title");
        String content = req.getParameter("content");

        //3.构造 blog 对象
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());

        //4.将blog对象插入数据库中
        BlogDao blogDao = new BlogDao();
        blogDao.insert(blog);

        //5.发布成功，重定向到列表页
        resp.sendRedirect("blog_list.html");
    }
}
