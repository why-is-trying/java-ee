function getLoginStatus() {
    $.ajax({
        type: 'get',
        url: 'login',
        success: function(body) {
            console.log("当前已经登陆过了");
        },
        error: function() {
            // 发现未登录直接访问，则调回登录页面
            console.log("当前未登录");
            location.assign('blog_login.html');
        }
    });
}