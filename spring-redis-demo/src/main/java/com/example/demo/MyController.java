package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class MyController {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/testString")
    @ResponseBody
    public String testString() {
        redisTemplate.opsForValue().set("key", "111");
        redisTemplate.opsForValue().set("key2", "222");
        redisTemplate.opsForValue().set("key3", "333");

        String value = redisTemplate.opsForValue().get("key");
        System.out.println("value=" + value);

        return "ok";
    }

    @GetMapping("/testList")
    @ResponseBody
    public String testList() {
        // 清除数据库
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
            return null;
        });

        redisTemplate.opsForList().leftPush("key", "111");

        String value = redisTemplate.opsForList().rightPop("key");
        System.out.println(value);

        return "ok";
    }

    @GetMapping("/testSet")
    @ResponseBody
    public String testSet() {
        // 清除数据库
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
            return null;
        });

        redisTemplate.opsForSet().add("key", "111", "222", "333");

        String value = redisTemplate.opsForSet().randomMember("key");
        System.out.println(value);

        return "ok";
    }

    @GetMapping("/testHash")
    @ResponseBody
    public String testHash() {
        // 清除数据库
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
            return null;
        });

        redisTemplate.opsForHash().put("key", "f1", "111");
        redisTemplate.opsForHash().put("key", "f2", "222");
        redisTemplate.opsForHash().put("key", "f3", "333");
        redisTemplate.opsForHash().put("key", "f4", "444");

        String value = (String) redisTemplate.opsForHash().get("key", "f1");
        System.out.println(value);

        return "ok";
    }

    @GetMapping("/testZSet")
    @ResponseBody
    public String testZSet() {
        // 清除数据库
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
            return null;
        });

        redisTemplate.opsForZSet().add("key", "zhangsan", 10);
        redisTemplate.opsForZSet().add("key", "lisi", 20);
        redisTemplate.opsForZSet().add("key", "liubei", 30);
        redisTemplate.opsForZSet().add("key", "guanfei", 40);

        Set<String> members = redisTemplate.opsForZSet().range("key", 0, -1);
        System.out.println(members);

        return "ok";
    }
}
