import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:将数据保存到文件中
 * User: 15158
 * Date: 2022-11-22
 * Time: 16:15
 */
@WebServlet("/messageFile")
public class MessageWallFile extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    private String filePath = "d:/pCode/messages/message.txt";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json; charset=utf-8");
        List<Message> list = load();
        resp.getWriter().write(objectMapper.writeValueAsString(list));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //将请求数据转换为对象
        Message message = objectMapper.readValue(req.getInputStream(),Message.class);
        //将数据保存到文件中
        save(message);
        resp.setStatus(200);
        System.out.println("提交数据成功："+message.getFrom()+"对"+message.getTo()+"说"+message.getMessage());
    }

    private List<Message> load() {
        System.out.println("从文件中读取数据：");
        List<Message> list = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))){
            while (true) {
                String line = bufferedReader.readLine();
                if(line == null) {
                    break;
                }
                String[] messages = line.split("\t");
                Message message = new Message();
                message.setFrom(messages[0]);
                message.setTo(messages[1]);
                message.setMessage(messages[2]);
                list.add(message);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("共读取数据 "+list.size()+" 条。");
        return list;
    }
    private void save(Message message) {
        System.out.println("向文件中写入数据：");
        try (FileWriter fileWriter = new FileWriter(filePath,true)){
            fileWriter.write(message.getFrom() + "\t" + message.getTo() + "\t" + message.getMessage() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
