import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * Description:封装连接数据库
 * User: 15158
 * Date: 2022-11-23
 * Time: 10:51
 */
//由于进行存储和读取数据时数据库只需要连接一次，则将其封装为单例模式（只有一个实例）
public class DBUtil {
    //数据源
    private static volatile DataSource dataSource = null;
    //构造方法设为私有的，程序员在使用的时候，不小心进行new的时候会有报错提醒
    private DBUtil() {}

    public static DataSource getDataSource() {
        //单列模式的饿汉模式会出现线程不安全情况，这里通过使用两个if一个锁保证线程安全
        //第一个if判断是为了提高效率
        if(dataSource == null) {
            synchronized (DBUtil.class) {
                if (dataSource == null) {
                    dataSource = new MysqlDataSource();
                    ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java105?characterEncoding=utf8&useSSL=false");
                    ((MysqlDataSource)dataSource).setUser("root");
                    ((MysqlDataSource)dataSource).setPassword("");
                }
            }
        }
        return dataSource;
    }
}
