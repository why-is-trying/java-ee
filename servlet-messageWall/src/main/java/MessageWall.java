import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:表白墙服务器端-将数据保存到数据库中
 * User: 15158
 * Date: 2022-11-22
 * Time: 13:18
 */
@WebServlet("/messageWall")
public class MessageWall extends HttpServlet {
    //ObjectMapper为Jackson的核心技术，可以调用它的方法
    public ObjectMapper objectMapper = new ObjectMapper();
    //使用集合保存提交信息
    //public ArrayList<Message> messageList = new ArrayList<Message>();

    //获取一个页面(含有提交信息)
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //显示设置浏览器的响应格式，不需要让浏览器猜测
        resp.setContentType("application/json; charset=utf-8");
        //将对象信息转为json字符串写入响应返回给浏览器
        //获取数据库中的数据
        List<Message> messageList = null;
        try {
            messageList = load();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.getWriter().write(objectMapper.writeValueAsString(messageList));
    }

    //保存用户提交信息到服务器端
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //将请求中的字符串转为对象
        Message message = objectMapper.readValue(req.getInputStream(),Message.class);
        //保存到数组中
        //messageList.add(message);
        //保存到数据库中
        try {
            save(message);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //设置响应状态
        resp.setStatus(200);
        //打印提交成功信息
        System.out.println("提交数据成功："+message.getFrom()+"对"+message.getTo()+"说"+message.getMessage());
    }

    //获取数据库中的数据
    private List<Message> load() throws SQLException {
        //1.获取数据源
//        DataSource dataSource = new MysqlDataSource();
//        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java105?characterEncoding=utf8&useSSL=false");
//        ((MysqlDataSource)dataSource).setUser("root");
//        ((MysqlDataSource)dataSource).setPassword("root");
        DataSource dataSource = DBUtil.getDataSource();
        //2.建立连接
        Connection connection = dataSource.getConnection();

        //3.构造SQL
        String sql = "select * from message";
        PreparedStatement statement = connection.prepareStatement(sql);

        //4.执行SQL,并将查询结果返回
        List<Message> listMessage = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Message message = new Message();
            message.setFrom(resultSet.getString("from"));
            message.setTo(resultSet.getString("to"));
            message.setMessage(resultSet.getString("message"));
            listMessage.add(message);
        }
        return listMessage;
    }
    //保存用户提交信息到数据库
    private void save(Message message) throws SQLException {
        //1.获取数据源
//        DataSource dataSource = new MysqlDataSource();
//        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java105?characterEncoding=utf8&useSSL=false");
//        ((MysqlDataSource)dataSource).setUser("root");
//        ((MysqlDataSource)dataSource).setPassword("root");
        DataSource dataSource = DBUtil.getDataSource();
        //2.建立连接
        Connection connection = dataSource.getConnection();

        //3.构造SQL
        String sql = "insert into message values(?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1,message.getFrom());
        statement.setString(2,message.getTo());
        statement.setString(3,message.getMessage());

        //4.执行SQL
        int ret = statement.executeUpdate();
        System.out.println("ret = " + ret);

        //5.关闭资源
        statement.close();
        connection.close();
    }
}
