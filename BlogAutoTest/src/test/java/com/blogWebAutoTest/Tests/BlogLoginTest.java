package com.blogWebAutoTest.Tests;

import com.blogWebAutoTest.common.AutotestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 20:45
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BlogLoginTest extends AutotestUtils {
    //调用AutotestUtils中的createDriver
    private static ChromeDriver driver = createDriver();
    //测试登录页面的一些共同步骤
    //1.获取浏览器对象
    //2.访问登录页面的URL
    @BeforeAll
    public static void baseControl() {
        driver.get("http://121.4.74.140:8080/blogSystem2/blog_login.html");
    }

    /**
     * 检查登录页面打开是否正确
     * 检查点：主页 写博客 元素是否存在
     */
    @Test
    @Order(1)
    public void loginPageLoadRight() throws IOException {
        driver.findElement(By.cssSelector("#username"));
        driver.findElement(By.cssSelector("#password"));
        getScreenShot(getClass().getName());
    }

    /**
     *检查正常登录是否成功
     */
    @ParameterizedTest
    @CsvSource({"张三,123","王一,123"})
    @Order(3)
    public void loginSuccess(String name,String password) throws IOException {
        //清空登录框，以防多组登录
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        //进行登录
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#login-button")).click();
        getScreenShot(getClass().getName());
        //如果跳转到博客列表页，才算登录成功
        driver.findElement(By.cssSelector("body > div.container > div.container-right > div:nth-child(1) > div.date"));
        driver.navigate().back();
    }

    /**
     * 登录失败检测
     */
    @ParameterizedTest
    @CsvSource({"张三,1234"})
    @Order(2)
    public void loginFail(String name,String password) throws IOException {
        //清空登录框，以防多组登录
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        //进行登录
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#login-button")).click();
        //登录失败情况
        String expect = "用户未注册或用户名密码错误！登录失败！";
        String actual = driver.findElement(By.cssSelector("body")).getText();
        getScreenShot(getClass().getName());
        Assertions.assertEquals(expect,actual);
        driver.navigate().back();
    }

}
