package com.blogWebAutoTest.Tests;

import com.blogWebAutoTest.common.AutotestUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-07
 * Time: 23:49
 */
//以防将驱动释放忘记，直接将驱动释放写成一个测试类
public class QuitDriverTest extends AutotestUtils {
    private static ChromeDriver driver = createDriver();
    @Test
    public void quitDriver() {
        driver.quit();
    }
}
