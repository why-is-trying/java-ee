package com.blogWebAutoTest.Tests;

import com.blogWebAutoTest.common.AutotestUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 20:44
 */
public class BlogListTest extends AutotestUtils {
    private static ChromeDriver driver = createDriver();

    @BeforeAll
    public static void baseControl() {
        driver.get("http://121.4.74.140:8080/blogSystem2/blog_list.html");
    }

    /**
     * 博客列表页面可以正常显示
     */
    @Test
    public void listPageLoadRight() throws IOException {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)"));
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)"));
        getScreenShot(getClass().getName());
    }
}
