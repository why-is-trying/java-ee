package com.blogWebAutoTest.Tests;

import com.blogWebAutoTest.common.AutotestUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 20:44
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BlogEditTest extends AutotestUtils {
    private static ChromeDriver driver = createDriver();

    @BeforeAll
    public static void baseControl() {
        driver.get("http://121.4.74.140:8080/blogSystem2/blog_edit.html");
    }

    /**
     *  检查编辑页面可以正常打开
     */
    @Test
    @Order(1)
    public void editPageLoadRight() throws IOException {
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(5)"));
        driver.findElement(By.cssSelector("body > div.nav > a:nth-child(6)"));
        getScreenShot(getClass().getName());
    }

    /**
     * 检查编写博客页面可以正常编写提交
     */
    @Test
    @Order(2)
    public void editAndSubmitBlog() throws IOException {
        String expect = "测试博客可以编写并提交";
        driver.findElement(By.cssSelector("#blog-title")).sendKeys(expect);
        //因为博客系统使用的是第三方软件，所以不能直接使用sendKeys
        driver.findElement(By.cssSelector("#editor > div.editormd-toolbar > div > ul > li:nth-child(17) > a > i")).click();
        driver.findElement(By.cssSelector("#submit")).click();
        getScreenShot(getClass().getName());
        //获取最后一条博客的标题文本，检查是否和预期一样
        String actual = driver.findElement(By.cssSelector("body > div.container > div.container-right > div:nth-child(5) > div.title")).getText();
        Assertions.assertEquals(expect,actual);
    }

}
