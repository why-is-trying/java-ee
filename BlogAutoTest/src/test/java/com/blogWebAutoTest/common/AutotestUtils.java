package com.blogWebAutoTest.common;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 20:45
 */
public class AutotestUtils {
    public static ChromeDriver driver;

    //1.创建驱动对象
    public static ChromeDriver createDriver() {
        if (driver == null) {
            driver = new ChromeDriver();
            //创建隐式等待
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        }
        return driver;
    }

    //通过格式化时间戳来动态的获取截图名
    public List<String> getTime() {
        //文件格式 20230307-225600
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyyMMdd-HHmmssSS");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyyMMdd");
        String fileName = simpleDateFormat1.format(System.currentTimeMillis());
        String dirName = simpleDateFormat2.format(System.currentTimeMillis());
        List<String > list = new ArrayList<>();
        list.add(dirName);
        list.add(fileName);
        return list;
    }
    /**
     * 获取屏幕截图，把所有的用例执行的结果保存下来
     */
    public void getScreenShot(String str) throws IOException {
        List<String> list = getTime();
        //dir+fileName
        String fileName = "./src/test/java/com/blogWebAutoTest/"+list.get(0)+"/"+str+"_"+list.get(1)+".png";
        File srcFile = driver.getScreenshotAs(OutputType.FILE);
        //把屏幕截图生成的文件放到指定的路径
        FileUtils.copyFile(srcFile,new File(fileName));
    }
}
