package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-11
 * Time: 10:33
 */
@Controller
@ResponseBody
public class IndexController {
    private String username = "张三";
    private String password = "123";
    @RequestMapping("/login")
    public String login(String username,String password) {
        if (this.username.equals(username) && this.password.equals(password)) {
            return "登录成功！";
        }
        return "登录失败";
    }
}
