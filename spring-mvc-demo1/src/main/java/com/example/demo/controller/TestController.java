package com.example.demo.controller;

import com.example.demo.Model.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-09
 * Time: 20:28
 */
@Controller //让框架启动的时候加载当前类（只有加载的类，别人才可以看见）
@ResponseBody //告诉程序返回的是一个数据而非页面
@Slf4j
public class TestController {
    //@RequestMapping(value = "/hi",method = RequestMethod.POST)
    //@PostMapping("/hi")
    @GetMapping("/hi")
    public String sayHi(String name) {
        return "hello,"+name;
    }

    @GetMapping("/user")
    public String showUser(User user) {
        return user.toString();
    }

    @GetMapping("/time")
    public String showTime(@RequestParam(value = "t",required = false) String startTime,
                           @RequestParam("t2") String endTime) {
        return "开始时间："+startTime + " | 结束时间："+endTime;
    }

    @PostMapping("/json-user")
    public String showJSONUser(@RequestBody User user) {
        return user.toString();
    }

    @RequestMapping("/login/{username}/{password}")
    public String showURL(@PathVariable("username") String username,@PathVariable("password") String password) {
        return username+":"+password;
    }

    //上传文件
    @RequestMapping("/upfile")
    public String upFile(@RequestPart("myfile")MultipartFile file) throws IOException {
        //只能保存一张图片
        String path = "D:\\Picture\\img.png";
        //保存文件
        file.transferTo(new File(path));
        return path;
    }
    @RequestMapping("/myupfile")
    public String myUpFile(@RequestPart("myfile")MultipartFile file) {
        //根目录+唯一文件名+文件后缀
        String path = "D:\\Picture\\";
        path += UUID.randomUUID().toString().replace("-","");
        path += file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        return path;
    }

    //Spring MVC(Spring Web)内置了HttpServletRequest 和 HttpServletResponse
    @GetMapping("/getparam")
    public String getParam(HttpServletRequest req) {
        return req.getParameter("username");
    }
    //获取Cookie
    @RequestMapping("/getck")
    public String getCookie(HttpServletRequest req) {
        //servlet的方式是得到所有的cookie
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            log.error(cookie.getName()+":"+cookie.getValue());
        }
        return "get cookie";
    }
    //可以获得单个Cookie
    @RequestMapping("/getck2")
    public String getCookie2(@CookieValue("zhangsan") String value) {
        return "Cookie value:"+value;
    }

    //获取header
    @RequestMapping("/getua")
    public String getUA(@RequestHeader("User-Agent") String userAgent) {
        return userAgent;
    }

    //设置和获取session
    @RequestMapping("/setsess")
    public String setSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("userinfo","userinfo");
        return "Set Session Success";
    }
    @RequestMapping("/getsess")
    public String getSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session!=null && session.getAttribute("userinfo")!=null) {
            return (String)session.getAttribute("userinfo");
        } else {
            return "暂无 session 信息";
        }
    }
    @RequestMapping("/getsess2")
    public String getSession2(@SessionAttribute(value = "userinfo",required = false)String userinfo) {
        return userinfo;
    }
}
