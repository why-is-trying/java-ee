package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-10
 * Time: 15:55
 */
@Controller
//@ResponseBody
public class SendController {
    @RequestMapping("/index")
    public String getIndex() {
        return "/index.html";
    }

    //请求重定向
    @RequestMapping("/index1")
    public String index1() {
        return "redirect:/index.html";
    }
    //请求转发
    @RequestMapping("/index2")
    public String index2() {
        return "forward:/index.html";
    }
}
