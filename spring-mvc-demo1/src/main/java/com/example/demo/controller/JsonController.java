package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-11
 * Time: 10:07
 */
@Controller
@ResponseBody
public class JsonController {
    @RequestMapping("/json")
    public HashMap<String,String> jsonBean() {
        HashMap<String,String> map = new HashMap<>();
        map.put("java","new");
        map.put("mysql","数据库");
        map.put("cpp","++");
        return map;
    }
}
