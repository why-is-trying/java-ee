package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-11
 * Time: 10:20
 */
@Controller
@ResponseBody
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        return "hello world";
    }
}
