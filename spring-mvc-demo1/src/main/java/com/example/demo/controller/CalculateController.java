package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-11
 * Time: 9:54
 */
@Controller
@ResponseBody
public class CalculateController {
    @RequestMapping("/calc")
    public String calc(Integer num1,Integer num2) {
        if(num1 == null || num2 == null) {
            return "参数错误";
        }
        return "结果是："+(num1+num2);
    }
}
