package com.example.demo.Model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-09
 * Time: 23:16
 */
@Data
public class User {
    private int id;
    private String name;
    private int age;
}
