import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:上传文件
 * User: 15158
 * Date: 2022-11-23
 * Time: 13:10
 */
@MultipartConfig
@WebServlet("/upload")
public class UploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取请求给定name的文件
        Part file = req.getPart("myFile");
        //获取提交的文件名
        System.out.println(file.getSubmittedFileName());
        //获取提交的文件类型
        System.out.println(file.getContentType());
        //获取文件大小
        System.out.println(file.getSize());
        //将提交文件写入磁盘文件
        file.write("d:/pCode/messages/upload.jpg");
        resp.getWriter().write("upload ok");
    }
}
