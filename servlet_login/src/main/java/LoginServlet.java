import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:登录请求处理
 * User: 15158
 * Date: 2022-11-23
 * Time: 12:18
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        //获取请求参数
        String userName = (String) req.getParameter("userName");
        String passWord = (String) req.getParameter("passWord");
        if("admin".equals(userName) && "123".equals(passWord)) {
            //登录成功
            //设置登录次数
            //获取session，如果没有则创建新的键值对添加到哈希表中，服务器将sessionId给set-cookie返回
            HttpSession httpSession = req.getSession(true);
            httpSession.setAttribute("userName",userName);
            httpSession.setAttribute("count",0);
            //登录成功跳转到index页面
            resp.sendRedirect("index");
        } else {
            //登录失败
            resp.getWriter().write("用户名/密码错误，请检查后重新登陆！！！");
            return;
        }
    }
}
