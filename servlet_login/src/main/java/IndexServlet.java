import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:登陆成功处理
 * User: 15158
 * Date: 2022-11-23
 * Time: 12:18
 */
@WebServlet("/index")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html; charset=utf-8");
        //获取session
        HttpSession session = req.getSession(false);
        if(session==null) {
            resp.getWriter().write("您尚未登录，请登录后访问！");
            resp.sendRedirect("login.html");
        }else {
            //获取用户名
            String userName = (String) session.getAttribute("userName");
            //获取上一次登录次数
            int count = (int) session.getAttribute("count");
            count += 1;
            session.setAttribute("count",count);
            resp.getWriter().write("<h3>登录成功！欢迎"+userName+"先生/女士，这是您第"+count+"次登录！</h3>");
        }
    }
}
