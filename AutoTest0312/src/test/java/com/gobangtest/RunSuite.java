package com.gobangtest;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({GoBangRegistTest.class, GoBangLoginTest.class, GoBangGameHallTest.class, GoBangGameRoomTest.class, QuiteDriverTest.class})
public class RunSuite {
}
