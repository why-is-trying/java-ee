package com.gobangtest;

import com.common.AutoTestUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoBangGameHallTest extends AutoTestUtils {
    // 1.创建驱动
    private static EdgeDriver driver = createEdgeDriver();
    // 2.提取共同操作
    @BeforeAll
    public static void baseControl() {
        driver.get("http://127.0.0.1:8081/game-hall.html");
    }
    /**
     * 3.检测页面是否正常
     * 检查点：展示用户信息的版面、匹配按钮
     */
    @Test
    @Order(1)
    public void LoadPageHallRight() throws IOException {
        driver.findElement(By.cssSelector("#screen"));
        driver.findElement(By.cssSelector("#match-button"));
        getScreenShot(getClass().getName());
    }
    /**
     * 4.检测用户是否登录
     * 检查点：用户信息是否为null
     */
    @Test
    @Order(2)
    public void isLogin() throws IOException {
        String message = driver.findElement(By.cssSelector("#screen")).getText();
        System.out.println(message);
        // 使用 split 方法拆分 message，以空格分隔
        String[] parts = message.split("\\s+");
        String playerName = null;
        // 遍历拆分后的字符串数组，查找玩家信息
        for (String part : parts) {
            // 判断是否包含玩家信息关键词
            if (part.startsWith("玩家：")) {
                // 提取玩家信息，假设玩家信息格式为 "玩家：[玩家名称]"
                playerName = part.substring(4); // 获取玩家名称，去掉前缀 "玩家："
                System.out.println("玩家信息：" + playerName);
                break; // 如果找到玩家信息，就不再继续查找
            }
        }
        Assertions.assertNotEquals(null, playerName);
        getScreenShot(getClass().getName());
    }
    /**
     * 5.检测登录状态下匹配按钮是否可以正常变化
     * 检查点：点击后出现弹窗
     */
    @Test
    @Order(3)
    public void isMatch() throws IOException, InterruptedException {
        // 1.先登录
        driver.get("http://127.0.0.1:8081/login.html");
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#username")).sendKeys("张三");
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
        driver.findElement(By.cssSelector("#submit")).click();
        // 2.进入到游戏大厅页面后，点击匹配按钮
        driver.findElement(By.cssSelector("#match-button")).click();
        // 3.获取点击匹配后的信息
        String actual = "匹配中...(点击停止)";
        getScreenShot(getClass().getName());
        String clickAfter = driver.findElement(By.cssSelector("#match-button")).getText();
        // 4.判断
        Assertions.assertEquals(clickAfter, actual);
        // 5.再次点击
        driver.findElement(By.cssSelector("#match-button")).click();
        String clickAgain = driver.findElement(By.cssSelector("#match-button")).getText();
        // 6.判断
        actual = "开始匹配";
        Assertions.assertEquals(clickAgain, actual);
        getScreenShot(getClass().getName());
    }
}
