package com.gobangtest;

import com.common.AutoTestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoBangLoginTest extends AutoTestUtils {
    // 创建驱动
    private static EdgeDriver driver = createEdgeDriver();

    // 测试登录页面的共同步骤
    // 1.获取浏览器对象
    // 2.访问登录页面的URL
    @BeforeAll
    public static void baseControl() {
        driver.get("http://127.0.0.1:8081/login.html");
    }

    /**
     * 检查登录页面打开是否正常
     * 检查点：主页 用户名 密码等元素是否存在
     */
    @Test
    @Order(1)
    public void LoginPageLoadRight() throws IOException {
        driver.findElement(By.cssSelector("#username"));
        driver.findElement(By.cssSelector("#password"));
        getScreenShot(getClass().getName());
    }

    /**
     * 检查登录是否成功
     */
    @ParameterizedTest
    @CsvSource({"张三,123","李四,123"})
    @Order(3)
    public void loginSuccess(String name, String password) throws IOException {
        // 清空登录框，以防多组登录
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        // 进行登录
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#submit")).click();
        getScreenShot(getClass().getName());
        // 如果跳转到游戏大厅，才算登录成功
        if (driver.getCurrentUrl().contains("game-hall")) {
            // 登录成功
            driver.findElement(By.cssSelector("#match-button"));
            System.out.println("登录成功！");
            driver.navigate().back();
        } else {
            System.out.println("登录失败！");
        }
    }

    /**
     * 登录失败检测
     */
    @ParameterizedTest
    @CsvSource({"张三2,1234"})
    @Order(2)
    public void loginFail(String name, String password) throws IOException {
        //登录失败
        try {
            // 清空登录框，以防多组登录
            driver.findElement(By.cssSelector("#username")).clear();
            driver.findElement(By.cssSelector("#password")).clear();

            //登录
            driver.findElement(By.cssSelector("#username")).sendKeys(name);
            driver.findElement(By.cssSelector("#password")).sendKeys(password);
            getScreenShot(getClass().getName());
            driver.findElement(By.cssSelector("#submit")).click();

            //获取警告框文本
            String expect = "登录失败！";
            Alert alert = driver.switchTo().alert();
            String actual = alert.getText();
            System.out.println(actual);
            alert.accept();

            Assertions.assertEquals(expect, actual);
        } catch (TimeoutException e) {
            System.out.println("登录失败：未出现警告框");
        }
    }
}
