package com.gobangtest;

import com.common.AutoTestUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

//防止忘记驱动释放，直接将驱动释放写成一个测试类
public class QuiteDriverTest extends AutoTestUtils {
    private static EdgeDriver edgeDriver = createEdgeDriver();
    private static ChromeDriver chromeDriver = createChromeDriver();
    @Test
    public void quitDriver() {
        edgeDriver.quit();
        chromeDriver.quit();
    }
}
