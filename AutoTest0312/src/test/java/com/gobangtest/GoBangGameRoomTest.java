package com.gobangtest;

import com.common.AutoTestUtils;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoBangGameRoomTest extends AutoTestUtils {
    // 创建驱动
    private static EdgeDriver edgeDriver = createEdgeDriver();
    private static ChromeDriver chromeDriver = createChromeDriver();

    // 将两个不同系统的用户匹配进同一游戏房间
    public void userLogin() {
        // user1
        edgeDriver.get("http://127.0.0.1:8081/login.html");
        edgeDriver.findElement(By.cssSelector("#username")).sendKeys("六1");
        edgeDriver.findElement(By.cssSelector("#password")).sendKeys("123");
        edgeDriver.findElement(By.cssSelector("#submit")).click();
        // user2
        chromeDriver.get("http://127.0.0.1:8081/login.html");
        chromeDriver.findElement(By.cssSelector("#username")).sendKeys("王五");
        chromeDriver.findElement(By.cssSelector("#password")).sendKeys("123");
        chromeDriver.findElement(By.cssSelector("#submit")).click();
        // user1和user2进同一游戏房间
        edgeDriver.findElement(By.cssSelector("#match-button")).click();
        chromeDriver.findElement(By.cssSelector("#match-button")).click();
    }

    /**
     * 测试游戏对战页面
     * 检查点：有棋盘、提示版元素
     */
    @Test
    @Order(1)
    public void testGameBoard() {
        // 不同系统的两个用户匹配到同一个房间
        userLogin();
        // 测试游戏对战页面
        edgeDriver.findElement(By.cssSelector("#chess"));
        chromeDriver.findElement(By.cssSelector("#chess"));
    }

    /**
     * 测试棋盘
     * 检查点：赢的一方，screen显示：你赢了!
     */
    @Test
    @Order(2)
    public void testChessBoard() {
        // 1.获取edge浏览器中的canvas
        WebElement canvas1 = edgeDriver.findElement(By.cssSelector("#chess"));
        WebElement canvas2 = chromeDriver.findElement(By.cssSelector("#chess"));

        int x = canvas2.getLocation().getX();
        int y = canvas2.getLocation().getY();
        System.out.println("(" + x + "," + y+ ")");

        Actions actions = new Actions(chromeDriver);
        actions.moveToElement(canvas2, x + 225, y + 225).click().perform();
    }
}
