package com.gobangtest;

import com.common.AutoTestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class GoBangRegistTest extends AutoTestUtils {
    // 1.先获取driver
    private static EdgeDriver driver = createEdgeDriver();

    // 2.注册页面的共有操作
    // 获取浏览器对象；访问注册页面的URL
    @BeforeAll
    public static void baseControl() {
        driver.get("http://127.0.0.1:8081/register.html");
    }

    /**
     * 3.检查页面是否可以正常打开
     * 检查点：注册名 确认密码元素是否存在
     */
    @Test
    @Order(1)
    public void registerLoadRight() throws IOException {
        driver.findElement(By.cssSelector("#username"));
        driver.findElement(By.cssSelector("#password"));
        driver.findElement(By.cssSelector("#password2"));
        getScreenShot(getClass().getName());
    }

    /**
     * 4.检查是否注册成功
     * 注册失败：alert("")（用户名/密码为空；密码和确认密码不一致；数据库已经有该用户）
     * 如果页面跳转到登录页面，则表明注册成功
     */
    @ParameterizedTest
    @CsvSource({"刘云,123,1234"})
    @Order(2)
    public void registerFail(String name, String password, String password2) throws IOException {
        //清空登录框，以防多组登录
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password2")).clear();
        //进行注册
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#password2")).sendKeys(password2);
        driver.findElement(By.cssSelector("#submit")).click();
        //将driver转到alert
        Alert alert = driver.switchTo().alert();
        //获取弹窗中的提示信息
        String actual = alert.getText();
        if (actual.equals("请先输入用户名！")) {
            System.out.println("用户名为空！");
        } else if (actual.equals("请先输入密码！")){
            System.out.println("密码为空！");
        } else if (actual.equals("请确认密码！")) {
            System.out.println("再次输入密码为空！");
        } else if (actual.equals("两次密码输入不一致！")) {
            System.out.println("两次密码不一致！");
        } else if(actual.equals("抱歉：注册失败，请重试！")){
            System.out.println("已有该用户，请重试！");
        } else if(actual.equals("恭喜：注册成功，是否跳转到登录页面？")) {
            alert.dismiss();
            System.out.println("注册成功，跳转到登录页面！");
        } else {
            System.out.println("注册异常！");
        }
        alert.accept();
        getScreenShot(getClass().getName());
    }

    /**
     * 5.注册成功测试
     * 测试成功：alert:恭喜：注册成功，是否跳转到登录页面？
     */
    @ParameterizedTest
    @CsvSource("六3,123,123")
    @Order(3)
    public void registerSuccess(String name, String password, String password2) throws InterruptedException, IOException {
        //清空登录框，以防多组登录
        driver.findElement(By.cssSelector("#username")).clear();
        driver.findElement(By.cssSelector("#password")).clear();
        driver.findElement(By.cssSelector("#password2")).clear();
        //进行注册
        driver.findElement(By.cssSelector("#username")).sendKeys(name);
        driver.findElement(By.cssSelector("#password")).sendKeys(password);
        driver.findElement(By.cssSelector("#password2")).sendKeys(password2);
        getScreenShot(getClass().getName());
        driver.findElement(By.cssSelector("#submit")).click();
        Thread.sleep(1000);
        Alert alert = driver.switchTo().alert();
        String actual = alert.getText();
        if (actual.contains("注册成功")) {
            alert.accept();
            if (driver.getCurrentUrl().contains("login-html")) {
                System.out.println("注册成功！");
            }
        } else {
            alert.dismiss();
            System.out.println("注册失败！");
        }
        getScreenShot(getClass().getName());
    }
}
