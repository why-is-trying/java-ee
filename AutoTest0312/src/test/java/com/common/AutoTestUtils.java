package com.common;

import org.apache.commons.io.FileUtils;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class AutoTestUtils {
    public static EdgeDriver edgeDriver;
    public static ChromeDriver chromeDriver;

    // common1.创建驱动对象
    public static EdgeDriver createEdgeDriver() {
        if (edgeDriver == null) {
            edgeDriver = new EdgeDriver();
            // 创建隐式等待
            edgeDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        }
        return edgeDriver;
    }

    // commmon2.创建驱动对象
    public static ChromeDriver createChromeDriver() {
        if (chromeDriver == null) {
            chromeDriver = new ChromeDriver();
            chromeDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        }
        return chromeDriver;
    }

    // 通过格式化时间戳来动态的获取截图名
    public List<String> getTime() {
        // 文件格式 20240319-173421
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyyMMdd-HHmmsssSS");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyyMMdd");
        String fileName = simpleDateFormat1.format(System.currentTimeMillis());
        String dirName = simpleDateFormat2.format(System.currentTimeMillis());
        List<String> list = new ArrayList<>();
        list.add(dirName);
        list.add(fileName);
        return list;

    }

    // common2.获取屏幕截图，把所有的用例执行的结果保存下来
    public void getScreenShot(String str) throws IOException {
        List<String> list = getTime();
        //dir+fileName
        String fileName = "./src/test/java/com/testscreen/" + list.get(0) + "/" + str + "_" + list.get(1) + ".png";
        File srcFile = edgeDriver.getScreenshotAs(OutputType.FILE);
        // 把屏幕截图生成的文件放到指定的路径
        FileUtils.copyFile(srcFile, new File(fileName));
    }
}
