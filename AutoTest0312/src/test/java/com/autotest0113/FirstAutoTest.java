package com.autotest0113;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

import java.util.List;

public class FirstAutoTest {

    // 在浏览器中自动搜索java这个关键词
    public void javaTest() throws InterruptedException {
        EdgeOptions options = new EdgeOptions();
        options.addArguments("D:\\Program Files\\Java\\jdk-1.8\\bin\\msedgedriver.exe");
        EdgeDriver driver = new EdgeDriver(options);
        driver.get("https://www.baidu.com");
//        Thread.sleep(5000);
//        driver.findElement(By.cssSelector("#kw")).sendKeys("java");
//        Thread.sleep(5000);
//        driver.findElement(By.cssSelector("#su")).click();
//        Thread.sleep(5000);
        List<WebElement> eles = driver.findElements(By.className("hotsearch-item"));
        for (WebElement ele : eles) {
            System.out.println(ele.getText());
        }
        driver.quit();
    }
}
