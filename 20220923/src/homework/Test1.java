package homework;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * Description:20220813作业4
 * User: 15158
 * Date: 2022-09-23
 * Time: 17:10
 */
class SumOperator {
    long even = 0;//偶数和
    long odd = 0;//奇数和
    public void addEvenNum(int num) {
        even += num;
    }
    public void addOddNum(int num) {
        odd += num;
    }
    public long result() {
        System.out.println("偶数和："+even);
        System.out.println("奇数和："+odd);
        return even+odd;
    }
}
public class Test1 {
    public static void main(String[] args) {
        long beg = System.currentTimeMillis();

        int[] array = new int[1000_0000];
        Random random = new Random();

        //给数组赋值
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100)+1;
        }

        //线程1：求偶数下标和；线程2：求奇数下标和
        SumOperator sumOperator = new SumOperator();
        Thread t1 = new Thread(()->{
            for (int i = 0; i < array.length; i+=2) {
                sumOperator.addEvenNum(array[i]);
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 1; i < array.length; i+=2) {
                sumOperator.addOddNum(array[i]);
            }
        });
        t1.start();
        t2.start();


        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        long sum = sumOperator.result();
        System.out.println("数组和："+sum);
        System.out.println((end-beg)+"ms");
    }
}
