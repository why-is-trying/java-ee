package homework;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * Description:20220920作业2
 * User: 15158
 * Date: 2022-10-10
 * Time: 11:46
 */
public class Test5 {
    public static void main(String[] args) {
        //核心线程数：5  最大线程数：10  线程空闲时长：3  任务队列：100  拒绝策略：忽略最新任务
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5,10,3,
                TimeUnit.SECONDS,new LinkedBlockingQueue<>(100),
                new ThreadPoolExecutor.DiscardPolicy());
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "已执行.");
            },"Thread-"+(i+1)).start();
        }
    }
}
