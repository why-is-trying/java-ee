package homework;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * Description:20220925作业8
 * User: 15158
 * Date: 2022-10-12
 * Time: 10:03
 */
public class Test8 {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger counter = new AtomicInteger();
        for (int i = 0; i < 10000; i++) {
            Thread t = new Thread(()->{
                counter.getAndIncrement();
            });
            t.start();
            t.join();
        }
        /*Thread t1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                counter.getAndIncrement();
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                counter.getAndIncrement();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();*/
        System.out.println(counter.get());
    }
}
