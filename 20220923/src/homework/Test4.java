package homework;

/**
 * Created with IntelliJ IDEA.
 * Description:20220915作业1
 * User: 15158
 * Date: 2022-09-27
 * Time: 13:22
 */
public class Test4 {
    public static Object lock = new Object();
    public static volatile int count = 0;
    public static volatile int COUNTER = 0;
    public static void fun2() {
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                synchronized (lock) {
                    while (count%3 != 0) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print(Thread.currentThread().getName());
                    count++;
                    lock.notifyAll();
                }
            }
        },"A");

        Thread t2 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                synchronized (lock) {
                    while (count%3 != 1) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.print(Thread.currentThread().getName());
                    count++;
                    lock.notifyAll();
                }
            }
        },"B");

        Thread t3 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                synchronized (lock) {
                    while (count%3 != 2) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(Thread.currentThread().getName());
                    count++;
                    lock.notifyAll();
                }
            }
        },"C");
        t1.start();
        t2.start();
        t3.start();
    }
    public static void fun1() {
        for (int i = 0; i < 10; i++) {
            Thread t1 = new Thread(()->{
                System.out.print("A");
            });
            Thread t2 = new Thread(()->{
                try {
                    t1.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print("B");
            });
            Thread t3 = new Thread(()->{
                try {
                    t2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print("C");
            });
            t1.start();
            t2.start();
            t3.start();
            try {
                t1.join();
                t2.join();
                t3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        fun3();
    }

    public static Object lock1 = new Object();
    public static Object lock2 = new Object();
    public static Object lock3 = new Object();
    public static void fun3() {
        Thread t1 = new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    synchronized (lock1) {
                        lock1.wait();
                    }
                    System.out.print("A");
                    synchronized (lock2) {
                        lock2.notify();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    synchronized (lock2) {
                        lock2.wait();
                    }
                    System.out.print("B");
                    synchronized (lock3) {
                        lock3.notify();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    synchronized (lock3) {
                        lock3.wait();
                    }
                    System.out.println("C");
                    synchronized (lock1) {
                        lock1.notify();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t3.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (lock1) {
            lock1.notify();
        }
    }

}
