package homework;

import sun.awt.windows.ThemeReader;

import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * Description:20220925作业1
 * User: 15158
 * Date: 2022-10-12
 * Time: 9:41
 */
public class Test6 {
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(10);
        AtomicInteger counter = new AtomicInteger();
        Thread t1 = new Thread(()->{
            try {
                semaphore.acquire();
                counter.getAndIncrement();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(()->{
            try {
                semaphore.acquire();
                counter.getAndIncrement();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(counter.get());
    }
}
