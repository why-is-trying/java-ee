package homework;

/**
 * Created with IntelliJ IDEA.
 * Description:20220913作业2
 * User: 15158
 * Date: 2022-09-26
 * Time: 12:47
 */
public class Test3 {
    public static void main(String[] args) {
        Thread t3 = new Thread(()->{
            System.out.println("c");
        },"c");
        Thread t2 = new Thread(()->{
            try {
                t3.join();//阻塞t2,t3执行后再执行t2
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("b");
        },"b");
        Thread t1 = new Thread(()->{
            try {
                t2.join();//阻塞t1，t2执行后再执行t1
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("a");
        },"a");
        t3.start();
        t2.start();
        t1.start();
    }
}
