package threading;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * Description:标准库中的定时器
 * User: 15158
 * Date: 2022-09-29
 * Time: 18:20
 */
public class Demo16 {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("闹钟1");
            }
        },1000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("闹钟2");
            }
        },2000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("闹钟3");
            }
        },3000);

        System.out.println("闹钟开始");
    }
}
