package threading;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:线程安全之volatile
 * User: 15158
 * Date: 2022-09-26
 * Time: 11:31
 */
public class Demo11 {
    static class Counter {
        //volatile保证每次都真的直接从主内存中重新读取
        public volatile int count = 0;
        public void increase() {
            count++;
        }
    }
    public static void main(String[] args) {
        Counter counter = new Counter();
        Thread t1 = new Thread(()->{
            //如果count不使用volatile，线程在执行的时候就编译器默认优化只读一次内存后默认count=0
            while (counter.count == 0) {

            }
            System.out.println("线程t1结束");
        });
        t1.start();
        Thread t2 = new Thread(()->{
            System.out.println("请输入count=");
            Scanner sc = new Scanner(System.in);
            counter.count = sc.nextInt();
        });
        t2.start();
    }
}
