package threading;

import java.util.concurrent.Semaphore;

/**
 * Created with IntelliJ IDEA.
 * Description:JUC之信号量Semaphore
 * User: 15158
 * Date: 2022-10-12
 * Time: 9:38
 */
public class Demo23 {
    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(10);

        //申请资源
        semaphore.acquire();
        System.out.println("p操作");

        //释放资源
        semaphore.release();
        System.out.println("v操作");
    }
}
