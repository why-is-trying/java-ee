package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:线程的状态:就绪、阻塞
 * User: 15158
 * Date: 2022-09-24
 * Time: 18:21
 */
public class Demo9 {
    public static void main(String[] args) {
        Thread t = new Thread(()->{
            int n = 0;
            for (int i = 0; i < 1_0000; i++) {
                n++;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println(t.getState());
        t.start();
        System.out.println(t.getState());

        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(t.getState());
        System.out.println("over!");
    }
}
