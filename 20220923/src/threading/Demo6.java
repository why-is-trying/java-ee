package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:测试线程的时间
 * User: 15158
 * Date: 2022-09-23
 * Time: 17:11
 */
public class Demo6 {
    public static final long count = 10_0000_0000l;
    public static void main(String[] args) {
        concurrency();
    }
    //串行
    public static void serial() {
        long beg = System.currentTimeMillis();
        int a = 0;
        for (long i = 0; i < count; i++) {
            a++;
        }
        a = 0;
        for (long i = 0; i < count; i++) {
            a++;
        }
        long end = System.currentTimeMillis();
        System.out.println((end-beg)+"ms");
    }
    //并行
    public static void concurrency() {
        long beg = System.currentTimeMillis();
        Thread t1 = new Thread(()->{
            int a = 0;
            for (long i = 0; i < count; i++) {
                a++;
            }
        });
        Thread t2 = new Thread(()->{
            int a = 0;
            for (long i = 0; i < count; i++) {
                a++;
            }
        });
        t1.start();
        t2.start();


        try {
            t1.join();//作用：t1执行结束后再执行main
            t2.join();//     t2执行结束后再执行main
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println((end-beg)+"ms");
    }
}
