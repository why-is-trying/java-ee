package threading;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * Description:JUC之原子类
 * User: 15158
 * Date: 2022-10-11
 * Time: 22:44
 */
public class Demo22 {
    public static void main(String[] args) throws InterruptedException {
        AtomicInteger count = new AtomicInteger();
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                count.getAndIncrement();//count++
                /*count.incrementAndGet();//++count;
                count.getAndDecrement();//count--;
                count.decrementAndGet();//--count*/
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 50000; i++) {
                count.getAndIncrement();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(count.get());
    }
}
