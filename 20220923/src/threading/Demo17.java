package threading;

import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * Description:实现一个定时器
 * User: 15158
 * Date: 2022-09-30
 * Time: 18:05
 */

/**
 * 定时器的构成:
 * 一个带优先级的阻塞队列
 * 队列中的每个元素是一个 Task 对象.
 * Task 中带有一个时间属性, 队首元素就是即将
 * 同时有一个 worker 线程一直扫描队首元素, 看队首元素是否需要执行
 */
//表示执行的任务
class MyTimerTask implements Comparable<MyTimerTask> {
    private Runnable runnable;//表示当前要执行的任务
    private long time;//表示任务执行的时间

    public MyTimerTask(Runnable runnable,long delay) {
        this.runnable = runnable;
        this.time = System.currentTimeMillis() + delay;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public void setRunnable(Runnable runnable) {
        this.runnable = runnable;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public int compareTo(MyTimerTask o) {
        return (int) (this.time - o.time);
    }
}
class MyTimer {
    private BlockingQueue<MyTimerTask> queue = new PriorityBlockingQueue<>();
    private Object locker = new Object();
    public MyTimer() {
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    //扩大加锁范围。保证take到wait之间是原子操作
                    synchronized (locker) {
                        //取出队首任务
                        MyTimerTask timerTask = queue.take();
                        if(System.currentTimeMillis() >= timerTask.getTime()) {
                            timerTask.getRunnable().run();
                        }else {
                            //时间还没到
                            queue.put(timerTask);
                            //如果锁加在这可能会出现 等待的时间大于新加入task。例如：等待后是两点，新加入的task是一点，所以需要将锁放大
                            locker.wait(timerTask.getTime() - System.currentTimeMillis());
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void schedule(Runnable runnable,long delay) throws InterruptedException {
        MyTimerTask timerTask = new MyTimerTask(runnable,delay);
        queue.put(timerTask);
        synchronized (locker) {
            //notify不会出现在take和wait之间
            locker.notify();
        }
    }
}
public class Demo17 {
    public static void main(String[] args) throws InterruptedException {
        MyTimer myTimer = new MyTimer();
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("闹钟开始1s");
            }
        },1000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("闹钟开始2s");
            }
        },2000);
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("闹钟开始3s");
            }
        },3000);
        System.out.println("闹钟开始");
    }
}
