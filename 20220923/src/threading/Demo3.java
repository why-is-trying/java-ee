package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:多线程创建方法三：使用匿名内部类，继承Thread
 * User: 15158
 * Date: 2022-09-23
 * Time: 16:43
 */
public class Demo3 {
    public static void main(String[] args) {
        Thread t = new Thread(){
            @Override
            public void run() {
                System.out.println("hello thread");
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

        while (true) {
            System.out.println("hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
