package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:线程安全之synchronized
 * User: 15158
 * Date: 2022-09-25
 * Time: 13:55
 */
class Counter {
    public int count = 0;
    public static Object locker = new Object();
    //加锁
    public synchronized void increase() {
        count++;
    }
    public void increase2() {
        synchronized (locker) {
            count++;
        }
        /*synchronized (this) {
            count++;
        }*/
    }
}
public class Demo10 {
    public static Counter counter = new Counter();
    public static Counter counter2 = new Counter();

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 1_0000; i++) {
                counter.increase2();
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 1_0000; i++) {
                counter.increase2();
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(counter.count);
    }
}
