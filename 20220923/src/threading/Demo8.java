package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:线程的基本用法
 * User: 15158
 * Date: 2022-09-24
 * Time: 16:35
 */
public class Demo8 {
    public static void main(String[] args) {
        //获取当前线程
        Thread t = Thread.currentThread();
        System.out.println(t.getName());
    }
    //线程等待：join
    public static void main5(String[] args) {
        Thread t = new Thread(()->{
            for (int i = 0; i < 5; i++) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        System.out.println("join前");
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("join后");
        System.out.println("over!");
    }
    //线程的中断：方法二：currentThread().isInterrupted()
    public static void main4(String[] args) {
        Thread t = new Thread(()->{
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("my thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //e.printStackTrace();
                    break;
                }
            }
        });
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t.interrupt();
        System.out.println("over!");
    }
    //线程的中断：方法一：设置标志位
    public static boolean isQuit = false;
    public static void main3(String[] args) {
        Thread t = new Thread(()->{
            while (!isQuit) {
                System.out.println("my thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isQuit = true;
        System.out.println("over!");
    }
    public static void main2(String[] args) {
        Thread t = new Thread(()->{
            while (true) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"这是我的线程");
        t.setDaemon(true);//设置后台线程
        t.start();
        System.out.println("over!");
    }
    public static void main1(String[] args) {
        Thread t = new Thread(()->{
            while (true) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"这是我的线程");
        t.start();
        System.out.println(t.getId());
        System.out.println(t.getName());
        System.out.println(t.getState());
        System.out.println(t.getPriority());
        System.out.println(t.isDaemon());
        System.out.println(t.isAlive());
        System.out.println(t.isInterrupted());
    }
}
