package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:实现阻塞队列之生产者消费者模型
 * User: 15158
 * Date: 2022-09-29
 * Time: 17:35
 */
class MyBlockingQueue {
    public int[] elem = new int[1000];
    public volatile int head = 0;
    public volatile int tail = 0;
    public volatile int usedSize = 0;

    public void put(int num) {
        synchronized (this) {
            while (this.usedSize>=this.elem.length) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            elem[this.tail] = num;
            this.tail++;
            if (tail >= this.elem.length) {
                tail = 0;
            }
            this.usedSize++;
            this.notify();
        }
    }
    public int take() {
        synchronized (this) {
            while (this.usedSize == 0) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int ret = elem[head];
            head++;
            if (head >= this.elem.length) {
                head = 0;
            }
            this.usedSize--;
            this.notify();
            return ret;
        }
    }
}
public class Demo15 {
    public static void main(String[] args) {
        MyBlockingQueue queue = new MyBlockingQueue();
        Thread produce = new Thread(() -> {
            int n = 1;
            while (true) {
                 queue.put(n);
                System.out.println("生产："+n);
                 n++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread consume = new Thread(() -> {
            while (true) {
                int ret = queue.take();
                System.out.println("消费："+ret);
            }
        });
        produce.start();
        consume.start();
    }
}
