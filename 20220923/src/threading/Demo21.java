package threading;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * Description:JUC之ReentrantLock
 * User: 15158
 * Date: 2022-10-11
 * Time: 22:41
 */
public class Demo21 {
    public static void main(String[] args) {
        ReentrantLock locker = new ReentrantLock();
        //ReentrantLock有一个小缺陷容易忘记unlock
        try {
            locker.lock();
        }finally {
            locker.unlock();
        }
    }
}
