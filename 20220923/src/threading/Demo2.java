package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:多线程创建方法二：实现Runnable接口
 * User: 15158
 * Date: 2022-09-23
 * Time: 16:32
 */
class MyRunnable implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
public class Demo2 {
    public static void main(String[] args) {
        MyRunnable runnable = new MyRunnable();
        Thread t1 = new Thread(runnable);//不同线程执行同样的任务
        Thread t2 = new Thread(runnable);
        t1.start();
        t2.start();
        while (true) {
            System.out.println("hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
