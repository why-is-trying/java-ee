package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:线程的run和start方法
 * User: 15158
 * Date: 2022-09-24
 * Time: 16:26
 */
class MyThreadDemo extends Thread {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
public class Demo7 {
    public static void main(String[] args) {
        MyThreadDemo myThreadDemo = new MyThreadDemo();
        //myThreadDemo.run();
        myThreadDemo.start();
        while (true) {
            System.out.println("hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
