package threading;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:单例模式之饿汉模式、懒汉模式
 * User: 15158
 * Date: 2022-09-28
 * Time: 11:35
 */

//单例类 饿汉模式
class Singleton {
    private static Singleton instance = new Singleton();
    public static Singleton getInstance() {
        return instance;
    }
    //将构造方法改为私有的,防止出现多个实例
    private Singleton() {

    }
}
//懒汉模式--线程不安全--如何修改使它变成线程安全的--通过加锁
class SingletonLazy {
    //加上volatile禁止指令重排序
    private volatile static SingletonLazy instance = null;
    public static synchronized SingletonLazy getInstance() {
        if(instance == null) {
            synchronized (SingletonLazy.class) {
                if (instance == null) {
                    instance = new SingletonLazy();
                }
            }
        }
        return instance;
    }
    private SingletonLazy(){}
}
public class Demo13 {
    public static void main(String[] args) {
        Singleton instance = Singleton.getInstance();
        SingletonLazy instance2 = SingletonLazy.getInstance();
    }
}
