package threading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * Description:标准库中的线程池
 * User: 15158
 * Date: 2022-09-30
 * Time: 19:21
 */
public class Demo18 {
    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newCachedThreadPool();
        threadPool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("线程池");
            }
        });
    }
}
