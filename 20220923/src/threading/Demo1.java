package threading;

import static java.lang.Thread.sleep;

/**
 * Created with IntelliJ IDEA.
 * Description:多线程创建方法一：继承Thread，实现run方法
 * User: 15158
 * Date: 2022-09-23
 * Time: 14:41
 */
class MyThread extends Thread {
    @Override
    public void run() {
        while (true) {
            System.out.println("hello thread");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class Demo1 {
    public static void main1(String[] args) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("1");
            }
        });
        t.start();
        System.out.println("2");
    }
    public static void main(String[] args) {
        MyThread t = new MyThread();
        t.start();

        while (true) {
            System.out.println("hello main");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
