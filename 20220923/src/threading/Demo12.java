package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:线程安全之wait、notify
 * User: 15158
 * Date: 2022-09-26
 * Time: 13:15
 */
public class Demo12 {
    public static Object object = new Object();
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            while (true) {
                synchronized (object) {
                    System.out.println("wait之前");
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("wait之后");
                }
            }
        });
        t1.start();
        Thread.sleep(500);
        Thread t2 = new Thread(() -> {
            while (true) {
                synchronized (object) {
                    System.out.println("notify之前");
                    object.notify();
                    System.out.println("notify之后");
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t2.start();
    }
}
