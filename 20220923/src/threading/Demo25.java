package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:死锁
 * User: 15158
 * Date: 2022-10-12
 * Time: 15:48
 */
public class Demo25 {
    public static void main(String[] args) {
        Object locker1 = new Object();
        Object locker2 = new Object();
        Thread t1 = new Thread(()->{
            System.out.println("t1获取locker1");
            synchronized (locker1) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("t1获取locker2");
                synchronized (locker2) {
                    System.out.println("t1获取两把锁成功");
                }
            }
        });
        Thread t2 = new Thread(()->{
            System.out.println("t2获取locker2");
            synchronized (locker2) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("t2获取locker1");
                synchronized (locker1) {
                    System.out.println("t2获取两把锁成功");
                }
            }
        });
        t1.start();
        t2.start();
    }
}
