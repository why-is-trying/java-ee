package threading;

import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * Description:CountDownLatch
 * User: 15158
 * Date: 2022-10-12
 * Time: 11:41
 */
public class Demo24 {
    public static void main(String[] args) throws InterruptedException {
        //是个选手参加比赛
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(()->{
                System.out.println("选手出发："+Thread.currentThread().getName());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("选手到达："+Thread.currentThread().getName());
                //撞线
                countDownLatch.countDown();
            });
            t.start();
        }
        //阻塞等待
        countDownLatch.await();
        System.out.println("比赛结束");
    }
}
