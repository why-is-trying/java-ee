package threading;

/**
 * Created with IntelliJ IDEA.
 * Description:多线程创建方法五：使用lambda表达式
 * User: 15158
 * Date: 2022-09-23
 * Time: 16:52
 */
public class Demo5 {
    public static void main(String[] args) {
        Thread t = new Thread(()->{
            while (true) {
                System.out.println("hello thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

        while (true) {
            System.out.println("hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
