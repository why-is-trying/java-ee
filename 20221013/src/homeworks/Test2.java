package homeworks;

import java.io.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:20220929作业4普通文件的复制
 * User: 15158
 * Date: 2022-10-14
 * Time: 17:39
 */
public class Test2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //1、输入源文件与目标文件
        System.out.println("请输入源文件：");
        File srcFile = new File(sc.next());

        System.out.println("请输入目标文件：");
        File desFile = new File(sc.next());
        //2、保证源文件是文件，目标文件的地址是有效的
        if(!srcFile.isFile()) {
            System.out.println("输入源文件有误");
            return;
        }
        if(!desFile.getParentFile().isDirectory()) {
            System.out.println("输入的目标文件有误");
            return;
        }
        //3、进行读写
        try(InputStream inputStream = new FileInputStream(srcFile);
            OutputStream outputStream = new FileOutputStream(desFile)) {
            while (true) {
                int ret = inputStream.read();
                if(ret == -1) {
                    break;
                }
                outputStream.write(ret);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
