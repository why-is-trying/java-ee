package homeworks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:20220929作业5
 * User: 15158
 * Date: 2022-10-14
 * Time: 18:18
 */
public class Test3 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        //1.输入指定目录
        System.out.println("输入指定目录：");
        File dic = new File(sc.next());
        //2.输入需要搜索的字符
        System.out.println("输入关键词：");
        String toFind = sc.next();
        //3.扫描文件夹
        scanDic(dic,toFind);
    }

    private static void scanDic(File dic, String toFind) throws IOException {
        File[] files = dic.listFiles();
        if(files == null) {
            return;
        }
        for (File f : files) {
            if(f.isDirectory()) {
                //是文件夹继续递归
                scanDic(f,toFind);
            }else {
                //查看该文件是不是符合
                checkFile(f,toFind);
            }
        }
    }

    private static void checkFile(File f, String toFind) throws IOException {
        //1.检查文件是否包含toFind
        if(f.getName().contains(toFind)) {
            System.out.println(f.getCanonicalPath()+" 中包含 "+toFind);
        }
        //2.检查文件中的内容是否包含toFind
        try (InputStream inputStream = new FileInputStream(f);
             Scanner scanner = new Scanner(inputStream)) {
            StringBuilder string = new StringBuilder();
            while (scanner.hasNextLine()) {
                string.append(scanner.nextLine());
            }
            if (string.indexOf(toFind) > -1) {
                System.out.println(f.getCanonicalPath() + " 中包含：" + toFind);
            }
        }
    }
}
