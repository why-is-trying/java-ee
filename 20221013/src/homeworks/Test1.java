package homeworks;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:20220929作业3
 * User: 15158
 * Date: 2022-10-14
 * Time: 17:13
 */
public class Test1 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        //1.输入指定目录
        System.out.println("请输入目录：");
        String dic = sc.next();
        File rootDic = new File(dic);
        //2.输入包含的字符
        System.out.println("请输入包含的字符：");
        String str = sc.next();
        //3、查找包含的字符
        scanDir(rootDic,str);
    }

    private static void scanDir(File rootDic, String str) throws IOException {
        //列出该目录的所有文件夹
        File[] files = rootDic.listFiles();
        //不存在则直接返回
        if (files == null) {
            return;
        }
        //扫描文件夹
        for(File file : files) {
            //如果是文件夹则继续进行递归
            if(file.isDirectory()) {
                scanDir(file,str);
            }else {
                //核查是否需要删除
                check(file,str);
            }
        }
    }

    private static void check(File file, String str) throws IOException {
        if(file.getName().contains(str)) {
            System.out.println("找到了包含 "+str+"的文件 "+file.getCanonicalPath()+" 是否需要删除？(y/n)");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.next();
            if(choice.equals("Y")||choice.equals("y")) {
                file.delete();
            }
        }
    }
}
