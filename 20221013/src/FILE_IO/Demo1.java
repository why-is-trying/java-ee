package FILE_IO;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:创建文件
 * User: 15158
 * Date: 2022-10-14
 * Time: 14:56
 */
public class Demo1 {
    public static void main(String[] args) throws IOException {
        File file = new File("test.txt");
        System.out.println(file.isFile());//是否是一个普通文件
        System.out.println(file.isDirectory());//是否是一个目录
        System.out.println(file.isAbsolute());//是否是绝对路径
        //创建文件
        file.createNewFile();
        System.out.println();
        System.out.println(file.isFile());//是否是一个普通文件
        System.out.println(file.isDirectory());//是否是一个目录
        System.out.println(file.isAbsolute());//是否是绝对路径
    }
}
