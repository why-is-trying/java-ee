package FILE_IO;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:重命名文件
 * User: 15158
 * Date: 2022-10-14
 * Time: 15:13
 */
public class Demo4 {
    public static void main(String[] args) throws IOException {
        File file1 = new File("Test/AAA/BBB/src.txt");
        File file2 = new File("Test/AAA/BBB/des.txt");
        file1.createNewFile();
        System.out.println(file1.exists());//file1要存在
        System.out.println(file2.exists());//file2需要不存在
        System.out.println(file1.renameTo(file2));
    }
}
