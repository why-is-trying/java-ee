package FILE_IO;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:字节流之InputStream与OutputStream
 * User: 15158
 * Date: 2022-10-14
 * Time: 15:47
 */
public class Demo5 {
    public static void main(String[] args) throws IOException {
        File file = new File("test.txt");
        file.createNewFile();
        //InputStream是一个抽象类，不能实例化
        InputStream inputStream = new FileInputStream("test.txt");
        while (true) {
            int b = inputStream.read();
            if(b == -1) {
                break;
            }
            System.out.println(b);
        }
        //一定要记住关闭资源,否则会造成资源泄漏
        inputStream.close();

        OutputStream outputStream = new FileOutputStream("test.txt");
        //新写入的会覆盖掉原有的数据
        outputStream.write(97);
        outputStream.write(98);
        outputStream.write(99);
        outputStream.close();
    }
}
