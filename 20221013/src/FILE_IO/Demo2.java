package FILE_IO;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * Description:创建文件夹以及多级文件夹
 * User: 15158
 * Date: 2022-10-14
 * Time: 15:05
 */
public class Demo2 {
    public static void main(String[] args) {
        File file = new File("Test/AAA/BBB");
        System.out.println(file.isDirectory());
        //创建一个目录
        file.mkdir();
        //创建多级目录
        file.mkdirs();
        System.out.println(file.isDirectory());
    }
}
