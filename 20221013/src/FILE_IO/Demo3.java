package FILE_IO;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * Description:删除文件
 * User: 15158
 * Date: 2022-10-14
 * Time: 15:11
 */
public class Demo3 {
    public static void main(String[] args) {
        File file = new File("test.txt");
        //删除文件
        file.delete();
        //进程退出时删除文件
        file.deleteOnExit();
    }
}
