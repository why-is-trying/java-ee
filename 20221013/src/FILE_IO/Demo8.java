package FILE_IO;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:使用PrintWriter
 * User: 15158
 * Date: 2022-10-14
 * Time: 16:40
 */
public class Demo8 {
    public static void main(String[] args) throws IOException {
        try(OutputStream outputStream = new FileOutputStream("./test.txt");
            PrintWriter writer = new PrintWriter(outputStream);){
            writer.println();
            writer.println("aaaa");
            writer.printf("a = %d\n",10);
        }
        /*OutputStream outputStream = new FileOutputStream("./test.txt");
        PrintWriter writer = new PrintWriter(outputStream);
        writer.println("第一行");
        writer.println("第二行");
        writer.close();*/
    }
}
