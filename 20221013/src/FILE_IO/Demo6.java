package FILE_IO;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Description:字符流之reader和writer
 * User: 15158
 * Date: 2022-10-14
 * Time: 16:20
 */
public class Demo6 {
    public static void main(String[] args) throws IOException {
        Reader reader = new FileReader("./test.txt");
        while (true) {
            int b = reader.read();
            if(b == -1) {
                break;
            }
            char c = (char) b;
            System.out.println(c);
        }
        reader.close();

        Writer writer = new FileWriter("./test.txt");
        writer.write("hello world");
        writer.close();
    }
}
