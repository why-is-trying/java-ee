package FILE_IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:使用Scanner
 * User: 15158
 * Date: 2022-10-14
 * Time: 16:35
 */
public class Demo7 {
    public static void main(String[] args) {
        //try结束后会自动调用对应对象的close
        try (InputStream inputStream = new FileInputStream("./test.txt")){
            Scanner scanner = new Scanner(inputStream);
            while (scanner.hasNext()) {
                System.out.println(scanner.next());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
