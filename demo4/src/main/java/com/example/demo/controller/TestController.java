package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/test")
@Controller
@ResponseBody
public class TestController {
    //@RequestMapping(value = "/hi", method = RequestMethod.POST) //路由注册
    @PostMapping("/hi")
    public String sayHi(String name) {
        //if (name == null || name.equals("")) {
        if (!StringUtils.hasLength(name)) {
            name = "张三";
        }
        return "您好，" + name;
    }
}