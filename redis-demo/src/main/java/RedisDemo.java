import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.params.SetParams;

public class RedisDemo {
    public static void test1(Jedis jedis) {
        System.out.println("get 和 set 的使用");
        // 1，先清空数据库
        jedis.flushDB();

        jedis.set("key", "111");
        jedis.set("key2", "222");

        SetParams params = new SetParams();
        params.ex(10);
        params.xx();// 只能在key存在的情况下设置成功
        //params.nx();// 只能在key不存在的情况下设置成功
        jedis.set("key", "333", params);

        String value = jedis.get("key");
        System.out.println("value = " + value);
    }

    public static void test2(Jedis jedis) {
        System.out.println("exists 和 del");
        jedis.flushAll();

        jedis.set("key", "111");
        jedis.set("key2", "222");

        boolean result = jedis.exists("key");
        System.out.println("result: " + result);

        long result2 = jedis.del("key");
        System.out.println("result2: " + result2);

        result = jedis.exists("key");
        System.out.println("result: " + result);
    }
    public static void main(String[] args) {
        // 连接到redis服务器
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");

        try (Jedis jedis = jedisPool.getResource()){
            // redis 的各种命令，都对应到 jedis 对象的各种方法
//            String pong = jedis.ping();
//            System.out.println(pong);
            //test1(jedis);
            test2(jedis);
        }
    }
}
