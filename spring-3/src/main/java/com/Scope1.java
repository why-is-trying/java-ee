package com;

import com.beans.BeanScope1;
import com.beans.BeanScope2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:测试设置bean的作用域-
 * User: 15158
 * Date: 2022-12-09
 * Time: 14:16
 */
public class Scope1 {
    public static void main(String[] args) {
        //1.获取context
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //2.取出bean
        BeanScope1 beanScope1 = context.getBean("beanScope1", BeanScope1.class);
        BeanScope2 beanScope2 = context.getBean("beanScope2", BeanScope2.class);
        //3.使用bean
        //未设置作用域--默认为单列模式--一个改变则都改
//        System.out.println(beanScope1.getUser3());
//        System.out.println(beanScope2.getUser3());
        //设置作用域--prototype--多例模式--一个改变不影响其他的
        System.out.println(beanScope1.getUser3());
        System.out.println(beanScope2.getUser3());
    }
}
