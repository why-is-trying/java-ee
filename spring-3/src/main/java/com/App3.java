package com;

import com.beans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:对象注入
 * User: 15158
 * Date: 2022-12-08
 * Time: 15:20
 */
public class App3 {
    public static void main(String[] args) {
        //一、属性注入
        //1.获取context
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //2.获取bean
        UserController2 userController2 = context.getBean("userController2", UserController2.class);
        //3.使用bean
        userController2.sayHi();

        //二、构造方法注入
        UserController3 userController3 = context.getBean("userController3", UserController3.class);
        userController3.sayHi();

        //三、方法注入
        UserController4 userController4 = context.getBean("userController4",UserController4.class);
        userController4.sayHi();
    }
}
