package com;

import com.beans.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:方法的注解与使用
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:52
 */
public class App2 {
    public static void main(String[] args) {
        //1.获取context
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //2.获取Bean，从bean中取存入的方法，直接使用方法的名称
        User user = context.getBean("user1",User.class);
        //3.使用Bean
        System.out.println(user);
    }
}
