package com;

import com.beans.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * Description:验证同一类型对象多次注入提取的方式（Resource+Autowired、Qualifier）
 * User: 15158
 * Date: 2022-12-09
 * Time: 12:47
 */
public class App4 {
    public static void main(String[] args) {
        //1.获取context
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //2.获取bean
        UserController userController = context.getBean("userController",UserController.class);
        //3.使用bean
        userController.sayYes();
    }
}
