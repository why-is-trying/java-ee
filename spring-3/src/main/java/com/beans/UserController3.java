package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:使用构造方法进行bean注入
 * User: 15158
 * Date: 2022-12-08
 * Time: 15:25
 */
@Controller
public class UserController3 {
    private UserService userService;

    //构造方法注入
    @Autowired
    public UserController3(UserService userService) {
        this.userService = userService;
    }

    public void sayHi() {
        userService.sayHi();
    }
}
