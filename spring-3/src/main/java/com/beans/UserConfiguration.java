package com.beans;

import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:五大类注解--Configuration--配置
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:12
 */
@Configuration
public class UserConfiguration {
    public void sayHi() {
        System.out.println("hello,Configuration");
    }
}
