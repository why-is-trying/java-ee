package com.beans;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:将方法存入spring中，使用方法的Bean注解
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:54
 */
@Component
public class UserBeans {
    //使用name对Bean重命名后，就不能用原来的方法名直接取出
    @Bean(name = {"userInfo","user1"})
    public User user1() {
        User user = new User();
        user.setName("张三");
        user.setAge(18);
        return user;
    }
    @Bean(name = "user2")
    public User user2() {
        User user = new User();
        user.setName("王五");
        user.setAge(19);
        return user;
    }

    //作用域为--prototype
    //两种方式--（1）@Scope("prototype") --（2）@Scope(ConfigurableBeanFactory.)
    @Bean(name = "user3")
//    @Scope("prototype")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public User user3() {
        User user = new User();
        user.setAge(500);
        user.setName("悟空");
        return user;
    }
}
