package com.beans;

import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA.
 * Description:五大类注解--Repository--仓库
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:04
 */
@Repository
public class UserRepoistory {
    public void sayHi() {
        System.out.println("hello,Repository!");
    }
}
