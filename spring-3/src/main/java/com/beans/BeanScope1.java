package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:设置Bean的作用域
 * User: 15158
 * Date: 2022-12-09
 * Time: 14:13
 */
@Component
public class BeanScope1 {
    @Autowired
    private User user3;

    public User getUser3() {
        User user = user3;
        user.setName("猪八戒");
        user.setAge(400);
        return user;
    }
}
