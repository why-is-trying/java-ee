package com.beans;

import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:五大类注解--Component--组件
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:16
 */
@Component
public class UserComponent {
    public void sayHi() {
        System.out.println("hello,component!");
    }
}
