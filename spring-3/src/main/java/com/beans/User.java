package com.beans;

/**
 * Created with IntelliJ IDEA.
 * Description:普通用户对象
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:53
 */
public class User {
    private String name;
    private int age;

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
