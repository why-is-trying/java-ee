package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:五大注解--Controller--控制器
 * User: 15158
 * Date: 2022-12-08
 * Time: 12:51
 */
@Controller
public class UserController {
    //同一个类型的User注入了两个两次，提取的时候可以有如下两种区分方式

    //方式一：Resource
//    @Resource(name = "user2")
//    private User user;

    //方式二：Autowired+Qualifier
    @Autowired
    @Qualifier(value = "user2")
    private User user;

    public void sayYes() {
        System.out.println("User->"+user);
    }
    public void sayHi() {
        System.out.println("hello,Controller!");
    }
}
