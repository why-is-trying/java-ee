package com.beans;

import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:五大类注解--Service--服务
 * User: 15158
 * Date: 2022-12-08
 * Time: 13:02
 */
@Service
public class UserService {
    public void sayHi() {
        System.out.println("hello,Service!");
    }
}
