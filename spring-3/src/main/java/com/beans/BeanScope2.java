package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Description:Bean作用域
 * User: 15158
 * Date: 2022-12-09
 * Time: 14:31
 */
@Component
public class BeanScope2 {
    @Autowired
    private User user3;

    public User getUser3() {
        return user3;
    }
}
