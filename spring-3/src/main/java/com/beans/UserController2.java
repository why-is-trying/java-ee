package com.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created with IntelliJ IDEA.
 * Description:对象注入
 * User: 15158
 * Date: 2022-12-08
 * Time: 15:18
 */
@Controller
public class UserController2 {
    //使用Autowired进行属性注入
    @Autowired
    private UserService userService;

    public void sayHi() {
        userService.sayHi();
    }
}
