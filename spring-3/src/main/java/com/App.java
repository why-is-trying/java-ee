package com;

import com.beans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.applet.AppletContext;

/**
 * Created with IntelliJ IDEA.
 * Description:五大类注解的创建于使用
 * User: 15158
 * Date: 2022-12-08
 * Time: 12:30
 */
public class App {
    public static void main(String[] args) {
        //1.获取context
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //2.获取bean,这里的第一个参数是bean的小驼峰形式
        UserController controller = context.getBean("userController",UserController.class);
        UserService service = context.getBean("userService",UserService.class);
        UserRepoistory repoistory = context.getBean("userRepoistory",UserRepoistory.class);
        UserConfiguration configuration = context.getBean("userConfiguration",UserConfiguration.class);
        UserComponent component = context.getBean("userComponent",UserComponent.class);
        //3.使用bean
        controller.sayHi();
        service.sayHi();
        repoistory.sayHi();
        configuration.sayHi();
        component.sayHi();
    }
}
