package org.example.factory;

import org.example.factory.impl.OrderDaoImpl;

public class OrderDaoFactory {
    public static OrderDao getOrderDao() {
        System.out.println("factory setup ...");
        return new OrderDaoImpl();
    }
}
