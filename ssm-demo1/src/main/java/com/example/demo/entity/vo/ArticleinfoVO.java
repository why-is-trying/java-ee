package com.example.demo.entity.vo;

import com.example.demo.entity.Articleinfo;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-12
 * Time: 19:46
 */
@Data
public class ArticleinfoVO extends Articleinfo implements Serializable {

    private final long serializableId = 1L;

    private String username;

    @Override
    public String toString() {
        return "ArticleinfoVO{" +
                "username='" + username + '\'' +
                "} " + super.toString();
    }
}
