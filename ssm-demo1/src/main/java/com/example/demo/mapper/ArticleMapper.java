package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import com.example.demo.entity.vo.ArticleinfoVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-12
 * Time: 19:50
 */
@Mapper
public interface ArticleMapper {
    List<Articleinfo> getAll();
    //通过id得到文章信息
    ArticleinfoVO getArticleById(Integer id);
}
