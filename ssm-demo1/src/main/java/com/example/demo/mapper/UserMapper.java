package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-11
 * Time: 16:50
 */
@Mapper
public interface UserMapper {
    //根据id查询用户信息
    Userinfo getUserById(@Param("id") Integer id);

    //查询所有用户信息
    List<Userinfo> getAll();

    //添加操作
    int add(Userinfo userinfo);

    //添加并返回用户的自增id
    int addGetId(Userinfo userinfo);

    //修改用户
    int upUserName(Userinfo userinfo);

    //删除根据Id
    int delById(@Param("id") Integer id);

    //通过用户名获取用户信息
    Userinfo getUserByName(@Param("username") String username);

    //按照用户id降序查询所有信息(需要使用￥符号)
    List<Userinfo> getUserByDesc(String order);

    //模糊查询
    List<Userinfo> findUserByName(String username);

    //<if><trim>标签添加
    int add2(Userinfo userinfo);

    //<where>标签
    List<Userinfo> getListByParam(@Param("username") String username,@Param("password") String password);

    //<set><update>标签
    int setById(@Param("username") String username,@Param("id")Integer id);

    //<foreach>标签
    int dels(List<Integer> ids);
}
