package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import com.example.demo.entity.vo.ArticleinfoVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-12
 * Time: 20:06
 */
@SpringBootTest
class ArticleMapperTest {
    @Autowired
    private ArticleMapper articleMapper;
    @Test
    void getArticleById() {
        Integer id = 1;
        ArticleinfoVO articleinfoVO = articleMapper.getArticleById(id);
        System.out.println(articleinfoVO);
    }

    @Test
    void getAll() {
        List<Articleinfo> list = articleMapper.getAll();
        System.out.println(list);
    }
}