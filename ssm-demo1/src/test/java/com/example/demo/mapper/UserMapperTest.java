package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-11
 * Time: 19:42
 */
@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;
    @Test
    void getUserById() {
        Userinfo userinfo = userMapper.getUserById(1);
        System.out.println(userinfo);
    }

    //查询所有用户信息
    @Test
    void getAll() {
        List<Userinfo> list = userMapper.getAll();
        Assertions.assertEquals(3,list.size());
    }

    @Test
    void add() {
        //伪代码，构建一个对象
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("张三");
        userinfo.setPassword("123");
        userinfo.setCreatetime(LocalDateTime.now());
        userinfo.setUpdatetime(LocalDateTime.now());
        //执行添加操作
        int result = userMapper.add(userinfo);
        System.out.println(result);
        System.out.println("用户Id:"+userinfo.getId());
        Assertions.assertEquals(1,result);
    }

    @Test
    void addGetId() {
        //伪代码，构建一个对象
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("陈久");
        userinfo.setPassword("123");
        userinfo.setCreatetime(LocalDateTime.now());
        userinfo.setUpdatetime(LocalDateTime.now());
        //执行添加操作
        int result = userMapper.addGetId(userinfo);
        System.out.println(result);
        System.out.println("用户Id:"+userinfo.getId());
        Assertions.assertEquals(1,result);
    }

    @Test
    void upUserName() {
        Userinfo userinfo = new Userinfo();
        userinfo.setId(5);
        userinfo.setUsername("陈鱼鱼");
        int result = userMapper.upUserName(userinfo);
        System.out.println(result);
    }

    @Test
    @Transactional //表示知测试但是不修改数据库中的数据
    void delById() {
        Integer id = 6;
        int result = userMapper.delById(id);
        System.out.println(result);
    }

    @Test
    void getUserByName() {
        Userinfo userinfo = userMapper.getUserByName("陈鱼鱼");
        System.out.println(userinfo);
    }

    @Test
    void getUserByDesc() {
        String order = "desc";
        List<Userinfo> list = userMapper.getUserByDesc(order);
        System.out.println(list);
    }

    @Test
    void findUserByName() {
        String username = "四";
        List<Userinfo> list = userMapper.findUserByName(username);
        System.out.println(list);
    }

    @Test
    void add2() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("陈二二");
        userinfo.setPassword("12345");
        userinfo.setPhoto("default.png");
        userMapper.add2(userinfo);
    }

    @Test
    void getListByParam() {
        List<Userinfo> list = userMapper.getListByParam(null,"123");
        System.out.println(list);
    }

    @Test
    void setById() {
        int ret = userMapper.setById("陈狗",9);
        System.out.println(ret);
    }

    @Test
    void dels() {
        List<Integer> ids = new ArrayList<>();
        ids.add(7);
        ids.add(9);
        int ret = userMapper.dels(ids);
        System.out.println(ret);
    }
}