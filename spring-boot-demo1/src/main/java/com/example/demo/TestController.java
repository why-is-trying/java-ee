package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-08
 * Time: 14:42
 */
@Controller //当前类为控制器
@ResponseBody //返回的是数据，而非页面
public class TestController {
    @Value("${myTest}")
    private String myTest;

    @Value("${myTest2}")
    private String myTest2;

    @Value("${myString}")
    private String myString;
    @Value("${myString2}")
    private String myString2;
    @Value("${myString3}")
    private String myString3;

    @Autowired
    private Student student;

    @PostConstruct
    public void postConstruct() {
        System.out.println(myString);
        System.out.println(myString2);
        System.out.println(myString3);
        System.out.println(student);
    }

    @RequestMapping("/hi") //路由注册
    public String sayHi(String name) {
        if (!StringUtils.hasLength(name)) {
            name = "张三";
        }
        return "您好，"+name;
    }

    @RequestMapping("/config")
    public String getConfig() {
        return myTest+"|"+myTest2;
    }
}
