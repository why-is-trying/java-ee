package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-08
 * Time: 21:29
 */
@Controller
@ResponseBody //用来设置当前返回的是数据 非页面
@RequestMapping("/user")
@Slf4j
public class UserController {
    //1.得到日志对象
    //private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping("/hi")
    public String sayHi() {
        //写日志
//        logger.trace("我是 trace");
//        logger.debug("我是 debug");
//        logger.info("我是 info");
//        logger.warn("我是 warn");
//        logger.error("我是 error");
        log.trace("我是 trace");
        log.debug("我是 debug");
        log.info("我是 info");
        log.warn("我是 warn");
        log.error("我是 error");
        return "hi,Spring Boot";
    }
}
