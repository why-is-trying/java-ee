package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-09
 * Time: 15:22
 */
@Service
@ResponseBody
@RequestMapping("/service")
@Slf4j
public class UserService {
    @RequestMapping("/hi")
    public String sayHi() {
        log.trace("我是 trace");
        log.debug("我是 debug");
        log.info("我是 info");
        log.warn("我是 warn");
        log.error("我是 error");
        return "hi,Service!";
    }
}
