package com.demo.component;

import com.demo.model.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Component
public class StudentBeans {
    //bean注解不能单独使用，需要配合五大类注解使用
//    @Bean(name = {"s1", "s2"})
    @Bean
    public Student student1() {
        //伪代码，构建对象
        Student student = new Student();
        student.setId(1);
        student.setName("张三");
        student.setAge(18);
        return student;
    }

    @Bean
    public Student student2() {
        //伪代码，构建对象
        Student student = new Student();
        student.setId(2);
        student.setName("李四");
        student.setAge(20);
        return student;
    }
}
