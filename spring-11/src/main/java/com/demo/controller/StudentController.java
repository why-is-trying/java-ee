package com.demo.controller;

import com.demo.model.Student;
import com.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class StudentController {
    // Autowired与Resource
//    相同点：都是用来实现依赖注入
//    区别：（1）功能支持不同：@Autowired支持属性注入、setter注入、构造方法u注入；@Resource支持属性注入、setter注入，但不支持构造方法注入。
//         （2）出身不同：@Autowired来自Spring框架，而@Resource来自与JDK。
//         （3）参数支持不同：@Autowired只支持required参数（是否是必须的），@Resource支持更多的参数设置。

    //    @Resource(name = "student1")
    @Autowired
    @Qualifier("student2")
    public Student student;
    public void sayHi2() {
        System.out.println(student.toString());
    }



    // 1.使用属性注入的方式获取Bean
    // （1）只适用与IoC容器；（2）更容易违背单一设计原则（针对对象是类）
//    @Autowired
//    或
//    @Resource
//    private StudentService studentService;

    // 2.set注入
    // set注入缺点：（1）不能注入不可变对象；（2）注入对象可被修改
    // 优点：更符合单一设计原则（针对对象方法级别）
//    private StudentService studentService;
//    @Autowired
//    public void setStudentService(StudentService studentService) {
//        this.studentService = studentService;
//    }

    // 3.构造方法注入
    // 优点：（1）可注入不可变对象；（2）注入对象不会被修改(加了final修饰符；构造方法是随着类加载只执行一次不像set有可能执行多次）；
    //      （3）注入对像会被完全初始化；（4）通用性更好。
    // 从 spring 4.x 之后官方推荐使用构造方法注入
    // 缺点：没有属性注入实现简单
    private StudentService studentService;
    //private final StudentService studentService;
    //只有一个对象的时候可以省略Autowired
    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    public void sayHi() {
        // 调用service方法
        studentService.sayHi();
    }
}
