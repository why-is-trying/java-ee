import com.demo.component.ArticleController;
import com.demo.component.UserComponent;
import com.demo.controller.StudentController;
import com.demo.controller.UserAdviceController;
import com.demo.controller.UserController;
import com.demo.model.Student;
import com.demo.service.StudentService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import javax.xml.bind.annotation.XmlAccessorOrder;

public class App {
    public static void main(String[] args) {
        // 1.得到spring对象
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        // 1.方式二：BeanFactory
//        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
//        // 2.从spring中取出bean对象
//        User user = (User) context.getBean("user");
//        User user2 = (User) context.getBean("user2");
//        // 2.方式二：获取bean,根据bean类型获取bean
////        User user3 = context.getBean(User.class);
//        // 3.方式三：根据bean名称+类型获取bean
////        User user4 = context.getBean("user", User.class);
//        // 3.使用bean（可选）
//        System.out.println(user.sayHi());
//        System.out.println(user);
//        System.out.println(user2);
//        System.out.println(user == user2);

//        ArticleController articleController = context.getBean("articleController", ArticleController.class);
//        System.out.println(articleController.sayHello());
//
//        UserComponent userComponent = context.getBean("userComponent",UserComponent.class);
//        System.out.println(userComponent.sayHi());
//
//        Student student = context.getBean("s1", Student.class);
//        System.out.println(student);

//        StudentController studentController = context.getBean("studentController", StudentController.class);
//        studentController.sayHi();
//        studentController.sayHi2();

        UserController uc = context.getBean("userController", UserController.class);
        uc.getUser();

        UserAdviceController ua = context.getBean("userAdviceController", UserAdviceController.class);
        ua.getUser();
    }
}
