package old;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 9:29
 */
public class Car {
    private Framework framework;
    public Car(int size) {
        this.framework = new Framework(size);
    }
    public void init() {
        System.out.println("执行了 Car");
        framework.init();
    }
}
