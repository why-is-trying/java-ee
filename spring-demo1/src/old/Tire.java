package old;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 9:18
 */
public class Tire {
    private int size = 20;

    //如果需要对尺寸改变,或者添加颜色等其他属性，就需要从轮胎开始一一加入，高耦合
    public Tire(int size) {
        this.size = size;
    }

    public void init() {
        System.out.println("轮胎尺寸："+size);
    }
}
