package loc;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 9:37
 */
public class Car {
    private Framework framework;
    public Car(Framework framework) {
        this.framework = framework;
    }
    public void init() {
        System.out.println("执行了 car");
        framework.init();
    }
}
