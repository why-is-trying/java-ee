package loc;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 9:35
 */
public class Bottom {
    private Tire tire;

    //不需要再new对象，直接将对象注入，解耦合
    public Bottom(Tire tire) {
        this.tire = tire;
    }
    public void init() {
        System.out.println("执行了 bottom");
        tire.init();
    }
}
