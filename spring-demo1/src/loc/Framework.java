package loc;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-06
 * Time: 9:36
 */
public class Framework {
    private Bottom bottom;
    public Framework(Bottom bottom) {
        this.bottom = bottom;
    }
    public void init() {
        System.out.println("执行了 framework");
        bottom.init();
    }
}
