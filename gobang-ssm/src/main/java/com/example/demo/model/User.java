package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-04-02
 * Time: 11:07
 */
@Data
public class User {
    private Integer userId;
    private String username;
    private String password;
    private Integer score;
    private Integer totalCount;
    private Integer winCount;
}
