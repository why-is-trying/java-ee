package com.example.demo.api;

import com.example.demo.game.MatchRequest;
import com.example.demo.game.MatchResponse;
import com.example.demo.game.Matcher;
import com.example.demo.game.OnlineUserManager;
import com.example.demo.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Created with IntelliJ IDEA.
 * Description: 通过这个类处理匹配功能中的 websocket 请求
 * User: 15158
 * Date: 2023-04-03
 * Time: 20:45
 */
@Component
public class MatchAPI extends TextWebSocketHandler {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private OnlineUserManager onlineUserManager;

    @Autowired
    private Matcher matcher;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        // 玩家上线，加入到onlineUserManager
        try {
            //1.先获取当前用户的身份信息
            // webSocketSession通过addInterceptors获取httpsession中的Attributes
            User user = (User) session.getAttributes().get("user");
            //2.判定当前用户是否是已经在线状态
            if (onlineUserManager.getFromGameHall(user.getUserId()) != null
                    || onlineUserManager.getFromGameRoom(user.getUserId()) != null) {
                //说明当前用户已经登录！
                //告知客户端重复登录
                MatchResponse response = new MatchResponse();
                response.setOk(true);
                response.setReason("当前禁止多开！");
                response.setMessage("repeatConnection");
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(response)));
                // 直接关闭有些太激进
                //session.close();
                return;
            }
            //3.将玩家设置为在线状态
            onlineUserManager.enterGameHall(user.getUserId(), session);
            System.out.println("玩家：" + user.getUsername() + " 进入游戏大厅！");
        } catch (NullPointerException e) {
            System.out.println("[MatchAPI.afterConnectionEstablished]当前用户未登录");
//            e.printStackTrace();
//            MatchResponse matchResponse = new MatchResponse();
//            matchResponse.setOk(false);
//            matchResponse.setReason("您尚未登录！不能进行后续匹配。");
//            // objectMapper将对象转换为JSON字符串
//            // TextMessage表示一个文本格式的websocket数据包
//            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
        }
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        // 处理开始匹配请求和处理停止匹配请求
        User user = (User) session.getAttributes().get("user");
        // 获取客户端发送给服务器的数据
        String payload = message.getPayload();
        // 将JSON搁置的字符串，转换为对象
        MatchRequest request = objectMapper.readValue(payload, MatchRequest.class);
        MatchResponse response = new MatchResponse();
        if (request.getMessage().equals("startMatch")) {
            //（1）进入匹配队列
            //（2）创建一个类表示匹配队列，把当前用户加进去
            matcher.add(user);
            response.setOk(true);
            response.setMessage("startMatch");
        } else if (request.getMessage().equals("stopMatch")) {
            // （1）退出匹配队列
            // （2）创建一个类表示匹配队列，把当前用户从队列中移除
            matcher.remove(user);
            // 移除之后，返回一个响应给客户端
            response.setOk(true);
            response.setMessage("stopMatch");
        } else {
            // 非法情况
            response.setOk(false);
            response.setReason("非法的匹配请求");
        }
        String jsonString = objectMapper.writeValueAsString(response);
        session.sendMessage(new TextMessage(jsonString));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        // 玩家下线，从 onlineUserManager 中删除
        try {
            User user = (User) session.getAttributes().get("user");
            WebSocketSession tmpSession = onlineUserManager.getFromGameHall(user.getUserId());
            // 保证退出的人和登录的人是一个人
            if (tmpSession == session) {
                onlineUserManager.exitGameHall(user.getUserId());
                System.out.println("玩家：" + user.getUsername() + " 退出游戏大厅！");
            }
            // 如果玩家正在匹配中，而 websocket 连接断开
            matcher.remove(user);
        } catch (NullPointerException e) {
            System.out.println("[MatchAPI.handleTransportError]当前用户未登录");
//            e.printStackTrace();
//            MatchResponse matchResponse = new MatchResponse();
//            matchResponse.setOk(false);
//            matchResponse.setReason("您尚未登录！不能进行后续匹配。");
//            // objectMapper将对象转换为JSON字符串
//            // TextMessage表示一个文本格式的websocket数据包
//            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        try {
            // 玩家下线，从 onlineUserManager 中删除
            User user = (User) session.getAttributes().get("user");
            WebSocketSession tmpSession = onlineUserManager.getFromGameHall(user.getUserId());
            // 保证退出的人和登录的人是一个人
            if (tmpSession == session) {
                onlineUserManager.exitGameHall(user.getUserId());
                System.out.println("玩家：" + user.getUsername() + " 退出游戏大厅！");
            }
            // 如果玩家正在匹配中，而 websocket 连接断开
            matcher.remove(user);
        } catch (NullPointerException e) {
            System.out.println("[MatchAPI.afterConnectionClosed]当前用户未登录");
//            e.printStackTrace();
//            MatchResponse matchResponse = new MatchResponse();
//            matchResponse.setOk(false);
//            matchResponse.setReason("您尚未登录！不能进行后续匹配。");
//            // objectMapper将对象转换为JSON字符串
//            // TextMessage表示一个文本格式的websocket数据包
//            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(matchResponse)));
        }
    }
}
