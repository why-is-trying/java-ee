package com.example.demo.controller;

import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-04-02
 * Time: 13:49
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserMapper userMapper;

    @RequestMapping("/login")
    public Object login(HttpServletRequest request, String username, String password) {
        // 1.从数据库中找到匹配用户
        User user = userMapper.selectByName(username);
        System.out.println("当前登录用户：user=" + user);
        if (user == null || !user.getPassword().equals(password)) {
            // 登录失败
            System.out.println("登录失败！");
            return new User();
        }
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("user", user);

        return user;
    }

    @RequestMapping("/register")
    public Object register(String username, String password) {
        try {
            User user = new User();
            user.setUsername(username);
            user.setPassword(password);
            userMapper.insert(user);
            return user;
        } catch (org.springframework.dao.DuplicateKeyException e) {
            User user = new User();
            return user;
        }

    }

    @RequestMapping("/getuserinfo")
    public Object getUserInfo(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession(false);
            User user = (User) session.getAttribute("user");
            // 拿着user对象，去数据库中找到最新数据
            User newUser = userMapper.selectByName(user.getUsername());
            return newUser;
        } catch (NullPointerException e) {
            return new User();
        }
    }
}
