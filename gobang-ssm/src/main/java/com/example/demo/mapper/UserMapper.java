package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-04-02
 * Time: 11:16
 */
@Mapper
public interface UserMapper {
    // 往数据库插入数据(用于注册)
    void insert(User user);

    // 根据用户名，来查询用户的详细信息(用于登录)
    User selectByName(@Param("username") String username);

    // 总场数+1，获胜场数+1，天梯分数+30
    void userWin(int userId);

    // 总场数+1，获胜场数不变，天梯分数-30
    void userLose(int userId);
}
