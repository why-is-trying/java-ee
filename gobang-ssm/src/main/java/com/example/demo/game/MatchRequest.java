package com.example.demo.game;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 表示一个 websocket 的匹配请求
 * User: 15158
 * Date: 2023-04-03
 * Time: 21:24
 */
@Data
public class MatchRequest {
    private String message = "";
}
