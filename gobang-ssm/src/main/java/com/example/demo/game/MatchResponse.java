package com.example.demo.game;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 表示一个 websocket 的响应请求
 * User: 15158
 * Date: 2023-04-03
 * Time: 21:25
 */
@Data
public class MatchResponse {
    private boolean ok;
    private String reason;
    private String message;
}
