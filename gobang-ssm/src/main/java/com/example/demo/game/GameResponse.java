package com.example.demo.game;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 表示落子响应
 * User: 15158
 * Date: 2023-04-05
 * Time: 11:23
 */
@Data
public class GameResponse {
    private String message;
    private int userId;
    private int row;
    private int col;
    private int winner;
}
