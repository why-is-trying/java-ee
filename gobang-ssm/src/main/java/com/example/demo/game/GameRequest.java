package com.example.demo.game;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 表示落子请求
 * User: 15158
 * Date: 2023-04-05
 * Time: 11:22
 */
@Data
public class GameRequest {
    private String message;
    private int userId;
    private int row;
    private int col;
}
