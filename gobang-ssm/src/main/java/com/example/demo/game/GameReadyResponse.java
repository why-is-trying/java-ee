package com.example.demo.game;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description: 客户端连接到游戏房间后，服务器返回的响应
 * User: 15158
 * Date: 2023-04-05
 * Time: 11:19
 */
@Data
public class GameReadyResponse {
    private String message;
    private boolean ok;
    private String reason;
    private String roomId;
    private int thisUserId;
    private int thatUserId;
    private int whiteUser;
}
