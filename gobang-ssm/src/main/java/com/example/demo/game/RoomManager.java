package com.example.demo.game;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-04-04
 * Time: 15:11
 */
@Component
public class RoomManager {
    private ConcurrentHashMap<String, Room> rooms = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, String> userIdRoomId = new ConcurrentHashMap<>();

    // 添加到房间
    public void add(Room room, int userId1, int userId2) {
        rooms.put(room.getRoomId(), room);
        userIdRoomId.put(userId1, room.getRoomId());
        userIdRoomId.put(userId2, room.getRoomId());
    }

    // 从房间移除
    public void remove(String roomId, int userId1, int userId2) {
        rooms.remove(roomId);
        userIdRoomId.remove(userId1);
        userIdRoomId.remove(userId2);
    }

    // 通过roomId获取房间
    public Room getRoomByRoomId(String roomId) {
        return rooms.get(roomId);
    }

    public Room getRoomByUserId(int userId) {
        String roomId = userIdRoomId.get(userId);
        if (roomId == null) {
            return null;
        }
        return rooms.get(roomId);
    }
}
