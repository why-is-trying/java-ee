package com.example.demo.game;

import com.example.demo.DemoApplication;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description: 游戏房间
 * User: 15158
 * Date: 2023-04-04
 * Time: 13:13
 */
@Data
public class Room {
    private String roomId;
    private User user1;
    private User user2;

    // 先手方的id
    private int whiteUser;
    private static final int MAX_ROW = 15;
    private static final int MAX_COL = 15;
    // 二维数组表示棋盘
    // 0:表示未落子
    // 1:表示玩家1的位置
    // 2:表示玩家2的位置
    private int[][] board = new int[MAX_ROW][MAX_ROW];

    private ObjectMapper objectMapper = new ObjectMapper();

    //@Autowired 不可以通过注解注入
    private OnlineUserManager onlineUserManager;

    //@Autowired 不可以通过注解注入
    private RoomManager roomManager; //用于房间销毁

    private UserMapper userMapper;

    public Room() {
        // 生成唯一字符串表示 房间Id
        roomId = UUID.randomUUID().toString();
        // 通过入口类记录的context手动获取到前面的RoomManager和OnlineUserManager
        onlineUserManager = DemoApplication.context.getBean(OnlineUserManager.class);
        roomManager = DemoApplication.context.getBean(RoomManager.class);
        userMapper = DemoApplication.context.getBean(UserMapper.class);
    }

    // 通过这个方法来处理一次落子操作
    public void putChess(String reqJson) throws IOException {
        // 1.记录当前落子的位置
        GameRequest request = objectMapper.readValue(reqJson, GameRequest.class);
        GameResponse response = new GameResponse();
        // 判断当前这个子是玩家1还是玩家2落下的
        int chess = request.getUserId() == user1.getUserId() ? 1 : 2;
        int row = request.getRow();
        int col = request.getCol();
        if (board[row][col] != 0) {
            System.out.println("当前位置(" + row + ", " + col + ")已经有子了！");
            return;
        }
        board[row][col] = chess;
        // 2.打印出棋盘信息（方便观察局势和验证胜负关系）
        printBoard();
        // 3.进行胜负判定
        int winner = checkWinner(row, col, chess);
        // 4.给房间中的所有客户端返回响应
        response.setMessage("putChess");
        response.setUserId(request.getUserId());
        response.setRow(row);
        response.setCol(col);
        response.setWinner(winner);

        // 要想给用户发送websocket数据就需要获取到这个用户的websocketsession
        WebSocketSession session1 = onlineUserManager.getFromGameRoom(user1.getUserId());
        WebSocketSession session2 = onlineUserManager.getFromGameRoom(user2.getUserId());
        // 非空效验
        if (session1 == null) {
            // 玩家1下线了，则玩家2获胜
            response.setWinner(user2.getUserId());
            System.out.println("玩家1掉线！");
        }
        if (session2 == null) {
            // 玩家2下线了，则玩家1获胜
            response.setWinner(user1.getUserId());
            System.out.println("玩家2掉线！");
        }
        // 把响应构造成Json字符串，并通过session返回
        String respJson = objectMapper.writeValueAsString(response);
        if (session1 != null) {
            session1.sendMessage(new TextMessage(respJson));
        }
        if (session2 != null) {
            session2.sendMessage(new TextMessage(respJson));
        }
        // 5.如果当前胜负已分，这个房间就可以销毁了
        if (response.getWinner() != 0) {
            System.out.println("游戏结束！房间即将销毁！roomId=" + roomId + " 获胜方为：" + response.getWinner());
            // 更新获胜方和失败方的信息
            int winUserId = response.getWinner();
            int loseUserId = response.getUserId() == user1.getUserId() ? user2.getUserId() : user1.getUserId();
            userMapper.userWin(winUserId);
            userMapper.userLose(loseUserId);
            // 销毁房间
            roomManager.remove(roomId, user1.getUserId(), user2.getUserId());
        }
    }

    private void printBoard() {
        //打印出棋盘
        System.out.println("[打印棋盘信息] " + roomId);
        System.out.println("===============================");
        for (int r = 0; r < MAX_ROW; r++) {
            for (int c = 0; c < MAX_COL; c++) {
                System.out.print(board[r][c] + " ");
            }
            System.out.println();
        }
        System.out.println("===============================");
    }

    private int checkWinner(int row, int col, int chess) {
        // 1.检查所有行
        for (int c = col - 4; c <= col; c++) {
            try {
                if (board[row][c] == chess
                        && board[row][c + 1] == chess
                        && board[row][c + 2] == chess
                        && board[row][c + 3] == chess
                        && board[row][c + 4] == chess) {
                    // 构成五子连珠
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                // 如果出现数组下标异常，则忽略
                continue;
            }
        }
        // 2.检查所有列
        for (int r = row - 4; r <= row; r++) {
            try {
                if (board[r][col] == chess
                        && board[r + 1][col] == chess
                        && board[r + 2][col] == chess
                        && board[r + 3][col] == chess
                        && board[r + 4][col] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        // 3.检查左对角线
        for (int r = row - 4, c = col - 4; r <= row && c <= col; r++, c++) {
            try {
                if (board[r][c] == chess
                        && board[r + 1][c + 1] == chess
                        && board[r + 2][c + 2] == chess
                        && board[r + 3][c + 3] == chess
                        && board[r + 4][c + 4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        // 4.检查右对角线
        for (int r = row - 4, c = col + 4; r <= row && c >= col; r++, c--) {
            try {
                if (board[r][c] == chess
                        && board[r + 1][c - 1] == chess
                        && board[r + 2][c - 2] == chess
                        && board[r + 3][c - 3] == chess
                        && board[r + 4][c - 4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        //胜负未分，就直接返回0
        return 0;
    }
}
