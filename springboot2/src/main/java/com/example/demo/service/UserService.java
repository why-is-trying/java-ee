package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2022-12-12
 * Time: 19:09
 */
@Controller
@ResponseBody
@Slf4j
public class UserService {
    @RequestMapping("/sayhi2")
    public void sayHi2() {
        //六种日志级别：从低到高
        log.trace("我是 trace");
        log.debug("我是 debug");
        log.info("我是 info");//info默认的日志级别
        log.warn("我是 warn");
        log.error("我是 error");
        //6、fatal：致命的日志。系统输出的日志，不能自定义打印
    }
}
