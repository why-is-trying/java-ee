package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-27
 * Time: 12:59
 */
@SpringBootTest
class ArticleMapperTest {

    @Autowired
    private ArticleMapper articleMapper;

    @Test
    void getArtCountByUid() {
        System.out.println(articleMapper.getArtCountByUid(1));
    }

    @Test
    void getListByPage() {
        List<Articleinfo> list = articleMapper.getListByPage(2, 0);
        List<Articleinfo> list2 = articleMapper.getListByPage(2, 2);
        System.out.println(list);
        System.out.println(list2);
    }
}