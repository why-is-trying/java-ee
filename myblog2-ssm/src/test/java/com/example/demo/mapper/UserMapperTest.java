package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-26
 * Time: 10:44
 */
@SpringBootTest
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    @Transactional
    void reg() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("张三2");
        userinfo.setPassword("123");
        System.out.println(userMapper.reg(userinfo));
    }

    @Test
    void getUserByName() {
        System.out.println(userMapper.getUserByName("鲤鱼"));
    }
}