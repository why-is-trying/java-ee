package com.example.demo.entity.vo;

import com.example.demo.entity.Userinfo;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-27
 * Time: 12:49
 */
@Data
public class UserinfoVO extends Userinfo {
    private Integer artCount;//个人用户的文章总数
}
