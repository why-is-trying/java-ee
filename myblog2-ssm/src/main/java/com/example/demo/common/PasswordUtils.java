package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-29
 * Time: 16:05
 */
public class PasswordUtils {

    //1.加盐并生成最终密码
    public static String encrypt(String password) {
        //(1)生成盐值(32位)
        String salt = UUID.randomUUID().toString().replace("-", "");
        //(2)生成加盐之后的密码(32位)
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        //(3)生成最终密码(32位+$+32位)
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    //2.方法一的重载
    public static String encrypt(String password, String salt) {
        //(1)生成加盐之后的密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    //3.验证密码
    public static boolean check(String inputPassword, String finalPassword) {
        if (StringUtils.hasLength(inputPassword) && StringUtils.hasLength(finalPassword) && finalPassword.length() == 65) {
            // （1）得到盐值
            String salt = finalPassword.split("\\$")[0];
            // （2）将输入的密码和得到的盐值进行加密
            String confirmPassword = PasswordUtils.encrypt(inputPassword, salt);
            // （3）验证密码是否相同
            return confirmPassword.equals(finalPassword);
        }
        return false;
    }

    public static void main(String[] args) {
        String inputPassword = "123456";
        String finalPassword = PasswordUtils.encrypt(inputPassword);
        System.out.println(PasswordUtils.check(inputPassword, finalPassword));
    }
}
