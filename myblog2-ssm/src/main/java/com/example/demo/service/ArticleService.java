package com.example.demo.service;

import com.example.demo.entity.Articleinfo;
import com.example.demo.mapper.ArticleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-27
 * Time: 13:01
 */
@Service
public class ArticleService {
    @Resource
    private ArticleMapper articleMapper;

    public Integer getArtCountByUid(Integer uid) {
        return articleMapper.getArtCountByUid(uid);
    }

    public List<Articleinfo> getMyList(Integer uid) {
        return articleMapper.getMyList(uid);
    }

    public int del(Integer id, Integer uid) {
        return articleMapper.del(id, uid);
    }

    public Articleinfo getArtDetailById(Integer id) {
        return articleMapper.getArtDetailById(id);
    }

    public Integer incrRCount(Integer id) {
        return articleMapper.incrRCount(id);
    }

    public Integer add(Articleinfo articleinfo) {
        return articleMapper.add(articleinfo);
    }

    public Integer update(Articleinfo articleinfo) {
        return articleMapper.update(articleinfo);
    }

    public List<Articleinfo> getListByPage(Integer psize, Integer offsize) {
        return articleMapper.getListByPage(psize, offsize);
    }

    public int getCount() {
        return articleMapper.getCount();
    }
}
