package com.example.demo.service;

import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-26
 * Time: 10:42
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public int reg(Userinfo userinfo) {
        return userMapper.reg(userinfo);
    }

    public Userinfo getUserByName(String username) {
        return userMapper.getUserByName(username);
    }

    public Userinfo getUserById(Integer id) {
        return userMapper.getUserById(id);
    }
}
