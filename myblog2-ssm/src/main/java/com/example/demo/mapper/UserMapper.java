package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-26
 * Time: 10:38
 */
@Mapper
public interface UserMapper {
    // 添加用户
    int reg(Userinfo userinfo);

    // 查询用户通过username
    Userinfo getUserByName(@Param("username") String username);

    // 通过uid得到用户信息
    Userinfo getUserById(@Param("id") Integer id);
}
