package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 15158
 * Date: 2023-03-27
 * Time: 12:56
 */
@Mapper
public interface ArticleMapper {
    //通过uid获取artCount
    int getArtCountByUid(@Param("uid") Integer uid);

    //通过uid获取articleinfo
    List<Articleinfo> getMyList(@Param("uid") Integer uid);

    //通过文章id删除文章
    int del(@Param("id") Integer id, @Param("uid") Integer uid);

    //通过文章id获取文章信息
    Articleinfo getArtDetailById(@Param("id") Integer id);

    //更新阅读量
    int incrRCount(@Param("id") Integer id);

    //添加文章
    int add(Articleinfo articleinfo);

    //修改文章
    int update(Articleinfo articleinfo);

    //通过页码和偏移量查询文章
    List<Articleinfo> getListByPage(@Param("psize") Integer psize, @Param("offsize") Integer offsize);

    //返回文章总条数
    int getCount();
}
