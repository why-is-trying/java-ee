// 注销
function logout() {
    if(confirm("确认注销？")) {
        jQuery.ajax({
            url:"/user/logout",
            type:"POST",
            data:{},
            success:function(result) {
                if(result!=null && result.code==200) {
                    location.href="/login.html";
                }
            }
        });
    }
}
// 获取url中的id信息
function getUrlValue(key) {
    var param = location.search;
    if(param.length > 1) {
        param = location.search.substring(1);
        var paramArr = param.split("&");
        for(var i=0;i<paramArr.length;i++) {
            var kv = paramArr[i].split("=");
            if(kv[0]==key) {
                return kv[1];
            }
        }
    }
    return "";
}